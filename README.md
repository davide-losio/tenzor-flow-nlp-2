# PESInet - Prosody Extraction by Sound Interpreting network

Pesinet è una rete neurale in grado di ricevere in input una frase pronunciata e classificarla basandosi sulla sua prosodia.

## Getting Started

Per istallare l'ultima versione del software è necessario clonare la repository:

```
git clone https://gitlab.com/davide-losio/tenzor-flow-nlp-2.git
```

### Prerequisites

Per utilizzare PESInet è necessario istallare le seguenti dipendenze:  

```
pip install pandas
pip install numpy
pip install -U scikit-learn
pip install tensorflow
```

## Running

PESInet è composta da tre moduli principali: solo Testo, solo Audio e Testo più Audio combinati insieme. A questi tre moduli si aggiunge un modello probabilistico, utilizzabile solo in fase di previsione e per il quale sono necessari delle precedenti sessioni di training dei due moduli separati.

Per avviare uno specifico modulo di PESInet è possibile utilizzare il parametro "features_type":

```
# avviare la rete in modalità solo audio
python PESInet_main.py --features_type snd

# avviare la rete in modalità solo testo
python PESInet_main.py --features_type txt

# avviare la rete in modalità audio più testo
python PESInet_main.py --features_type st

# avviare la rete per effettuare predizione probabilistica
python PESInet_main.py --features_type stp
```

Per quanto riguarda le modalità ad esclusione di quella probabilistica, sono possibili tre principali utilizzi della rete, settabili tramite il parametro "mode":

```
# avviare la rete in modalità training
python PESInet_main.py --mode TR

# avviare la rete in modalità cross-validation
python PESInet_main.py --mode CV

# avviare la rete in modalità prediction
python PESInet_main.py --mode P
```


Gli iperparametri per ciascuna modalità vanno settati nel file config.ini. 
È importante, ai fini del funzionamento, non
cambiare i parametri dei singoli moduli quando vengono uniti nella rete cobinata, audio
più testo, poichè questo ne pregiudicherebbe il funzionamento.

Per quel che riguarda la rete, CV è stato testato solo molto tempo fa, e solo sui singoli
moduli. Si consiglia di controllare e testare sia per il funzionamento coi singoli moduli
che ovviamente per la rete combinata. Data la modularità del codice, a livello teorico,
il funzionamento coi singoli moduli dovrebbe garantire anche il funzionamento della rete 
composta.

Per ciò che riguarda gli iter di utilizzo allenamento:

1 - Trainare i singoli moduli separatamente, o la configurazione combinata a scelta.
    settare save checkpoint True, sugli iper parametri se si desidera salvare l'apprendimento.

2 - I checkpoint file che permettono di riutilizzare la rete trainata a scopo di predittore saranno posti 
    nella cartella ckpt.

3 - La prediction session si occuperà automaticamente di effettuare predizione utilizzando
    i pesi e i parametri salvati nei checkpoint posti nella cartella ckpt.

È FONDAMENTALE che la rete in prediction sia esattamente uguale a quella trainata, altrimenti
non si potra procedere al restore automatico dei parametri appresi durante il training.

N.B la cartella di logs veniva usata per salvare i dati provenienti dal monitoraggio della
    rete con tensorboard, che è stato disattivato nella versione finale. 

### Utilities

La directory "utilities" contiene una serie di script utilizzati a contorno del funzionamento di PESInet, soprattutto legati alla creazione e gestione del dataset.

PESInet accetta in ingresso dataset nel formato tfrecords. I due script principali per la creazione dei dataset si utilizzano nel seguente modo:

```
# comando per creare il dataset audio a partire da tre distinte cartelle contenenti ciascuna una classe di files .csv
python PESInet_sound_csv_tfr.py statements_csv_dir exclamations_csv_dir questions_csv_dir out_tfrecords_file_name
```

```
# comando per creare il dataset testo a partire da tre distinte cartelle contenenti ciascuna una classe di files .txt
python PESInet_text_txt_tfr.py statements_txt_dir exclamations_txt_dir questions_txt_dir out_tfrecords_file_name
```

Per quanto riguarda la creazione del dataset testuale è necessario definire all'interno dello script il path al modello word embedding pre-trainato. Nel nostro caso è stato reperito da http://hlt.isti.cnr.it/wordembeddings/ ed utilizzato nel seguente modo:

```
model = gensim.models.Word2Vec.load(local_path + '/W2V_model/glove_WIKI')
```

Un'altra funzionalità utile è data da PESInet_directory_splitter.py, che permette di separare in maniera randomica una certa percentuale (di defaukt il 10%) dei file in una cartella "testset" definibile all'interno dello script. I parametri da definire in questo caso sono i posizionamenti delle tre cartelle contenenti i file delle diverse classi e la cartella di root del testset. Lo script ricreerà all'interno di questa cartella una separazione in base alla classe di appartenenza di ciascun file.

Si consiglia di lanciare prima lo script PESInet_directory_splitter.py, per dividere il trainset dal testset, e poi i due script PESInet_text_txt_tfr.py e PESInet_sound_csv_tfr.py.

Per generare un dataset comune di campioni audio e testo utilizzare lo script PESInet_combined_data_tfr.py.


