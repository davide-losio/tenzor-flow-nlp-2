# PESInet.
#
# Copyright (C) 2017, Davide Francesco Losio, Mauro Luchetti
#
# This file is part of PESInet.
#
#     PESInet is free software: you can redistribute it and/or modify
#     it under the terms of the GNU General Public License as published by
#     the Free Software Foundation, either version 3 of the License, or
#     (at your option) any later version.
#
#     PESInet is distributed in the hope that it will be useful,
#     but WITHOUT ANY WARRANTY; without even the implied warranty of
#     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#     GNU General Public License for more details.
#
#     You should have received a copy of the GNU General Public License
#     along with PESInet.  If not, see <http://www.gnu.org/licenses/>.
# ==========================================================================

import tensorflow as tf
from PESInet_Model import PESInet_model as Mod

import PESInet_predict_sess as Pps


class pred_sess(Pps.PredictSess):
    def init_sess(self):
        model = {}

        t_inputs = self.get_inputset("text_inputs", self.cfg.t_pred_path)
        s_inputs = self.get_inputset("sound_inputs", self.cfg.s_pred_path)

        t_iterator = t_inputs.make_one_shot_iterator()
        s_iterator = s_inputs.make_one_shot_iterator()

        t_batchf, t_batchl = t_iterator.get_next()
        s_batchf, s_batchl = s_iterator.get_next()

        txt_h_feat = self.get_h_feat(t_batchf, self.cfg.txt_cfg, self.cfg.txt_cfg.mode)
        snd_h_feat = self.get_h_feat(s_batchf, self.cfg.snd_cfg, self.cfg.snd_cfg.mode)

        h_feat = tf.concat([txt_h_feat, snd_h_feat], 1)
        h_feat = tf.nn.dropout(h_feat, 1.0)

        in_dimension = (self.cfg.txt_cfg.n_hidden[-1] + self.cfg.snd_cfg.n_hidden[-1]) * 2

        logits, _ = self.get_logits(h_feat, in_dimension, self.cfg, 'fully_connected_layer')

        softm = Mod.probabilities(logits)

        model['t_iterator'] = t_iterator
        model['s_iterator'] = s_iterator
        model['batchf'] = (t_batchf, s_batchf)
        model['batchl'] = t_batchl
        model['logits'] = logits
        model['softm'] = softm

        return model
