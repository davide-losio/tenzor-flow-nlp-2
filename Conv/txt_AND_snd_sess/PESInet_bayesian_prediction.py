# PESInet.
#
# Copyright (C) 2017, Davide Francesco Losio, Mauro Luchetti
#
# This file is part of PESInet.
#
#     PESInet is free software: you can redistribute it and/or modify
#     it under the terms of the GNU General Public License as published by
#     the Free Software Foundation, either version 3 of the License, or
#     (at your option) any later version.
#
#     PESInet is distributed in the hope that it will be useful,
#     but WITHOUT ANY WARRANTY; without even the implied warranty of
#     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#     GNU General Public License for more details.
#
#     You should have received a copy of the GNU General Public License
#     along with PESInet.  If not, see <http://www.gnu.org/licenses/>.
# ==========================================================================

import numpy as np
import tensorflow as tf
from PESInet_Model import PESInet_model as mod
from sklearn.metrics import confusion_matrix, accuracy_score, f1_score

import PESInet_predict_sess as pps


class PredSess(pps.PredictSess):

    def __init__(self, cfg):
        super(PredSess, self).__init__(cfg)

    def init_sess(self):

        model = {}

        t_inputs = self.get_inputset(self.cfg.txt_cfg, "text_inputs", self.cfg.t_pred_path)
        s_inputs = self.get_inputset(self.cfg.snd_cfg, "sound_inputs", self.cfg.s_pred_path)

        t_iterator = t_inputs.make_one_shot_iterator()
        s_iterator = s_inputs.make_one_shot_iterator()

        t_batchf, t_batchl = t_iterator.get_next()
        s_batchf, s_batchl = s_iterator.get_next()

        t_hfeat = self.get_h_feat(t_batchf, self.cfg.txt_cfg, "text")

        s_hfeat = self.get_h_feat(s_batchf, self.cfg.snd_cfg, "sound")

        s_pred = self.get_logits(s_hfeat, self.cfg.snd_cfg.n_hidden[-1] * 2, self.cfg, self.cfg.mode)
        t_pred = self.get_logits(t_hfeat, self.cfg.txt_cfg.n_hidden[-1] * 2, self.cfg, self.cfg.mode)
        s_prob = self.get_prob(s_pred)
        t_prob = self.get_prob(t_pred)

        h_feat = tf.concat([t_hfeat, s_hfeat], 1)

        h_feat = tf.nn.dropout(h_feat, self.keep_prob)

        in_dimension = (self.cfg.txt_cfg.n_hidden[-1] + self.cfg.snd_cfg.n_hidden[-1]) * 2

        logits, _ = self.get_logits(h_feat, in_dimension, self.cfg, 'fully_connected_layer')

        prob = self.get_prob(logits)

        if self.cfg.calc_probs == 'False':
            prior_prob = self.cfg.prior_probs
        else:
            prior_prob = self.calc_prior_prob()

        model['prob'] = prob
        model['s_prob'] = s_prob
        model['t_prob'] = t_prob
        model['prior_prob'] = prior_prob
        model['s_batchl'] = s_batchl
        model['s_batchf'] = s_batchf
        model['t_batchl'] = t_batchl

        return model

    def predict(self, ck_path, model):

        pred = model['prob']
        batchl = model['batchl']
        sound_pred = model['s_prob']
        s_batchl = model['s_batchl']
        s_batchf = model['s_batchf']
        text_pred = model['t_prob']
        t_batchl = model['t_batchl']


        with tf.Session() as sess:

            sess.run(tf.global_variables_initializer())
            # Restore variables from disk.

            saver = tf.train.Saver()
            saver.restore(sess, "./ckpt/whole_net_ckpt/")

            print("PESInet_Models restored.")

            y_true = []
            y_pred = []

            cnt = 0
            while True:
                try:
                    txt, t_lab = sess.run([pred, batchl])
                    y_true.append(np.argmax(t_lab))
                    y_pred.append(np.argmax(txt))
                    cnt += 1
                except tf.errors.OutOfRangeError:
                    break

            print(cnt)
            final_cm = confusion_matrix(y_true, y_pred)
            acc = accuracy_score(y_true=y_true, y_pred=y_pred)
            print(final_cm)
            print(acc)

            all_pitchs = []
            counter = 0
            while True:
                try:
                    snd, s_lab, feat = sess.run([sound_pred, s_batchl, s_batchf])
                    pitch = []
                    for i in feat[0]:
                        pitch.append(i[0])
                    all_pitchs.append(pitch)
                    counter += 1
                    if counter % 1000 == 0:
                        print('Step %d' % counter)
                except tf.errors.OutOfRangeError:
                    break

            all_pitchs = sorted(all_pitchs)

            with open('pitch_new.txt', 'a') as pitch_file:
                for el in all_pitchs:
                    pitch_file.write(str(el[:3]) + '\n')

            audio_recall_dict = {}
            audio_G_dict = {}
            text_G_dict = {}
            text_big_f_dict = {}

            for i in range(self.cfg.classes):
                audio_recall_dict[i] = self.cfg.s_conf[i][i] / np.sum(self.cfg.s_conf[i])

                for j in range(self.cfg.classes):
                    column = self.cfg.t_conf[:, j]
                    text_big_f_dict[i, j] = self.cfg.t_conf[i][j] / np.sum(column)

                    s_row = self.cfg.s_conf[i]
                    audio_G_dict[i, j] = self.cfg.s_conf[i][j] / np.sum(s_row)

                    t_row = self.cfg.t_conf[i]
                    text_G_dict[i, j] = self.cfg.t_conf[i][j] / np.sum(t_row)

            prior_prob = [1 / 3, 1 / 3, 1 / 3]

            counter = 0
            while True:
                try:
                    txt, snd, s_lab, t_lab = sess.run([text_pred, sound_pred, s_batchl, t_batchl])
                    txt = txt[0]
                    snd = snd[0]

                    if np.argmax(s_lab) != np.argmax(t_lab):
                        raise ValueError('Labels does not match at iteration %d' % counter)

                    predictions = []

                    for target_class in range(self.cfg.classes):

                        acceptance_pr = 0.0

                        for ca_idx in range(self.cfg.classes):
                            for ct_idx in range(self.cfg.classes):
                                big_g_testo = text_G_dict[ct_idx, target_class]
                                big_g_audio = audio_G_dict[ca_idx, target_class]
                                pred_audio = snd[ca_idx]
                                pred_text = txt[ct_idx]

                                acceptance_pr += (big_g_audio *
                                                  big_g_testo *
                                                  pred_audio *
                                                  pred_text *
                                                  1 / 3) / prior_prob[ca_idx] * prior_prob[ct_idx]

                        predictions.append(acceptance_pr)

                    self.write_results(predictions, s_lab)
                    y_pred.append(np.argmax(predictions))
                    y_true.append(np.argmax(s_lab))
                    counter += 1
                    if counter % 1000 == 0:
                        print('Step %d' % counter)
                except tf.errors.OutOfRangeError:
                    break
            final_cm = confusion_matrix(y_true, y_pred)
            acc = accuracy_score(y_true=y_true, y_pred=y_pred)
            f1 = f1_score(y_true=y_true, y_pred=y_pred, average='macro')
            print(final_cm)
            print(acc)
            print(f1)
            self.write_final_metrics(y_true=y_true, y_pred=y_pred)

    def write_final_metrics(self, y_true, y_pred):

        with open("final_metrics.txt", 'w') as text:

            final_cm = confusion_matrix(y_true=y_true, y_pred=y_pred)
            acc = accuracy_score(y_true=y_true, y_pred=y_pred)
            f1 = f1_score(y_true=y_true, y_pred=y_pred, average='macro')

            text.write('Conf matrix: ' + np.array2string(final_cm, separator=', ') + '\n')
            text.write('Accuracy: ' + str(acc) + '\n')
            text.write('F1: ' + str(f1) + '\n')

    def write_results(self, prediction, lab):
        with open("whole_net.txt", "a") as text:
            text.write("predicted " + str(prediction) + "||| labels " + str(lab) + "\n")

    def calc_prior_prob(self):
        inputs = self.get_inputset(self.cfg.snd_cfg, "sound_inputs", self.cfg.s_pred_path)

        iterator = inputs.make_one_shot_iterator()
        batchf, batchl = iterator.get_next()
        logits, _ = mod.logits(self.cfg.snd_cfg,
                               mod.high_level_feat(self.cfg.snd_cfg, batchf, 1.0),
                               self.cfg.snd_cfg.n_hidden[-1] * 2)
        softmax = mod.probabilities(logits)

        prior_prob = np.zeros(self.cfg.classes)

        # prediction session
        with tf.Session() as sess:
            sess.run(tf.global_variables_initializer())
            # Restore variables from disk.
            self.restore(sess, self.cfg.snd_cfg.ckpt_path, self.cfg.snd_cfg.mode)
            print("PESInet_Model restored.")
            counter = 0
            while True:
                try:
                    tmp_pred = sess.run(softmax)
                    prior_prob = np.add(prior_prob, tmp_pred)
                    counter += 1
                    if counter % 1000 == 0:
                        print('Step %d: prob: %s' % (counter, prior_prob))
                except tf.errors.OutOfRangeError:
                    break

            print('########## End prior prob computation')
            prior_prob = np.multiply(prior_prob, (1 / self.cfg.snd_cfg.dataset_size))
            prob_sum = np.sum(prior_prob)
            print('Prior prob final: %s ----- Prior sum final: %s' % (np.divide(prior_prob, prob_sum), prob_sum))

            if prob_sum >= 1:
                print(prob_sum)
                raise ValueError
        print("s: ", prob_sum, "prior: ", prior_prob)
        return np.divide(prior_prob, prob_sum)
