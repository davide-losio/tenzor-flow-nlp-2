# PESInet.
#
# Copyright (C) 2017, Davide Francesco Losio, Mauro Luchetti
#
# This file is part of PESInet.
#
#     PESInet is free software: you can redistribute it and/or modify
#     it under the terms of the GNU General Public License as published by
#     the Free Software Foundation, either version 3 of the License, or
#     (at your option) any later version.
#
#     PESInet is distributed in the hope that it will be useful,
#     but WITHOUT ANY WARRANTY; without even the implied warranty of
#     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#     GNU General Public License for more details.
#
#     You should have received a copy of the GNU General Public License
#     along with PESInet.  If not, see <http://www.gnu.org/licenses/>.
# ==========================================================================

import tensorflow as tf


class Dataset(object):

    def __init__(self, config, path):
        self.config = config
        self.dataset = tf.contrib.data.TFRecordDataset(path)
        self.dataset = self.dataset.map(self._parse_function)

        if self.config.shuffle_dataset == 'True':
            self.dataset = self.dataset.shuffle(buffer_size=self.config.dataset_size)

    def _parse_function(self, example_proto):

        features = {"features": tf.FixedLenFeature((), tf.string, default_value=""),
                    "labels": tf.FixedLenFeature((), tf.string, default_value="")}
        parsed_features = tf.parse_single_example(example_proto, features)
        features = tf.decode_raw(parsed_features["features"], tf.float32)
        labels = tf.decode_raw(parsed_features["labels"], tf.float32)
        features = tf.reshape(features, [-1, self.config.n_of_features])
        labels = tf.reshape(labels, [-1, self.config.classes])

        # in case we have one label for each data row of the same sample, we took just one of them.
        labels = labels[0]

        return features, labels

    def get_dataset(self):
        return self.dataset.padded_batch(self.config.batch_size,
                                         padded_shapes=([None, self.config.n_of_features], [self.config.classes]))

    def get_infinite_dataset(self):
        self.dataset = self.dataset.repeat()
        return self.dataset.padded_batch(self.config.batch_size,
                                         padded_shapes=([None, self.config.n_of_features], [self.config.classes]))

    def get_epoch_dataset(self):
        self.dataset = self.dataset.repeat(self.config.epochs)
        return self.dataset.padded_batch(self.config.batch_size,
                                         padded_shapes=([None, self.config.n_of_features], [self.config.classes]))


class Validationset(Dataset):

    def get_validationset(self):
        self.dataset = self.dataset.repeat()
        return self.dataset.padded_batch(self.config.validation_batch,
                                         padded_shapes=([None, self.config.n_of_features], [self.config.classes]))


class Testset(Dataset):

    def get_testset(self):
        self.dataset = self.dataset.repeat()
        return self.dataset.padded_batch(self.config.testset_size,
                                         padded_shapes=([None, self.config.n_of_features], [self.config.classes]))