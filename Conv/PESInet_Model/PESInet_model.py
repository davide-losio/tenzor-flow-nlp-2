# PESInet.
#
# Copyright (C) 2017, Davide Francesco Losio, Mauro Luchetti
#
# This file is part of PESInet.
#
#     PESInet is free software: you can redistribute it and/or modify
#     it under the terms of the GNU General Public License as published by
#     the Free Software Foundation, either version 3 of the License, or
#     (at your option) any later version.
#
#     PESInet is distributed in the hope that it will be useful,
#     but WITHOUT ANY WARRANTY; without even the implied warranty of
#     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#     GNU General Public License for more details.
#
#     You should have received a copy of the GNU General Public License
#     along with PESInet.  If not, see <http://www.gnu.org/licenses/>.
# ==========================================================================

import tensorflow as tf


def high_level_feat(cfg, features, keep_prob):
    conv_layers_outputs = []
    blstm_layers_outputs = []
    conv_strides = []
    conv_steps = []
    sequence_lengths = []

    conv_strides.append(cfg.stride)
    conv_steps.append(cfg.filter_rows)

    # calculating parameters characterizing the network
    # you have to do it before declaring the net, because at runtime
    # the network have to be already defined in its graph

    for i in range(cfg.conv_layers):
        conv_strides.append(conv_strides[i])
        conv_steps.append(conv_steps[i] + conv_strides[i])

    for i in range(cfg.conv_layers):
        conv_layers_outputs.append(conv_layer(cfg, conv_strides[i], conv_steps[i], features, i, keep_prob))
        sequence_lengths.append(tf.cast((length(features) - conv_steps[i] + conv_strides[i])/conv_strides[i], tf.int32))

    next_input = conv_layers_outputs[0]

    for i in range(cfg.blstm_layers):
        blstm_layers_outputs.append(blstm_layer(cfg, keep_prob, next_input, sequence_lengths[i], i))
        next_input = prepare_next_blstm_input(blstm_layers_outputs[i],
                                              conv_layers_outputs[i + 1],
                                              cfg,
                                              i)

    with tf.variable_scope('last-BLSTM'):
        forward_cell = make_cell(cfg.n_hidden[-1], keep_prob, cfg.n_of_filters[-1])
        backward_cell = make_cell(cfg.n_hidden[-1], keep_prob, cfg.n_of_filters[-1])

        (output_fw, output_bw), _ = tf.nn.bidirectional_dynamic_rnn(
            forward_cell,
            backward_cell,
            next_input,
            dtype=cfg.dtype,
            sequence_length=sequence_lengths[-1]
        )

    with tf.variable_scope('preparing_last_layer_input'):
        last_fw = _last_relevant(output_fw, sequence_lengths[-1])
        last_bw = output_bw[:, 0, :]
        last = tf.concat([last_fw, last_bw], 1)

    return last


def conv_layer(cfg, stride, w_steps, features, idx, keep_prob):
    with tf.variable_scope(str(w_steps) + '-1D-Convolution'):
        kernels = weight_variable(name='kernels',
                                  shape=[w_steps,
                                         cfg.n_of_features,
                                         cfg.n_of_filters[idx]])
        conv = tf.nn.conv1d(features, kernels, stride=stride, padding='VALID')
        biases = bias_variable(name='biases',
                               shape=[cfg.n_of_filters[idx]])
        pre_activation = tf.nn.bias_add(conv, biases)
        output = tf.nn.tanh(pre_activation, name='conv_features')
        output = tf.nn.dropout(output, keep_prob=keep_prob)

    return output


def make_cell(n_units, keep_prob, input_size, use_peepholes=False):
    cell = tf.nn.rnn_cell.LSTMCell(n_units, use_peepholes=use_peepholes)
    droped_out_cell = tf.nn.rnn_cell.DropoutWrapper(cell,
                                                    input_keep_prob=keep_prob,
                                                    output_keep_prob=keep_prob,
                                                    state_keep_prob=keep_prob,
                                                    input_size=input_size,
                                                    variational_recurrent=True,
                                                    dtype=tf.float32)
    return droped_out_cell


def blstm_layer(cfg, keep_prob, _input, seq_len, idx):

    with tf.variable_scope('BLSTM-' + str(idx)):
        forward_cell = make_cell(cfg.n_hidden[idx], keep_prob, cfg.n_of_filters[idx], False)
        backward_cell = make_cell(cfg.n_hidden[idx], keep_prob, cfg.n_of_filters[idx], False)

        (output_fw, output_bw), _ = tf.nn.bidirectional_dynamic_rnn(
            forward_cell,
            backward_cell,
            _input,
            dtype=cfg.dtype,
            sequence_length=seq_len
        )

        y = merge_fwd_bwd(output_fw, output_bw, idx, cfg)

    return y


def merge_fwd_bwd(h_state_fw, h_state_bw, idx, cfg):

    batch_size = tf.shape(h_state_fw)[0]

    with tf.name_scope('weights-' + str(idx)):
        blstm_weights_fw = weight_variable([cfg.n_hidden[idx], cfg.n_hidden[idx]], 'fw_BLSTM-' + str(idx))
        blstm_weights_bw = weight_variable([cfg.n_hidden[idx], cfg.n_hidden[idx]], 'bw_BLSTM-' + str(idx))

    h_state_fw = tf.reshape(h_state_fw, [-1, cfg.n_hidden[idx]])
    h_state_bw = tf.reshape(h_state_bw, [-1, cfg.n_hidden[idx]])

    y_fw = tf.matmul(h_state_fw, blstm_weights_fw)
    y_bw = tf.matmul(h_state_bw, blstm_weights_bw)

    y_a1 = tf.add(y_fw, y_bw)

    return tf.reshape(y_a1, [batch_size, -1, cfg.n_hidden[idx]])


def pair_steps(batch_size, n_hidden_units, blstm_out):

    zeros = tf.zeros([batch_size, 1, n_hidden_units], dtype=tf.float32)
    blstm_out_2_the_revenge = tf.concat([blstm_out, zeros], 1)
    blstm_out = tf.concat([zeros, blstm_out], 1)
    blstm_out_paired = tf.concat([blstm_out, blstm_out_2_the_revenge], 2)
    blstm_out_paired = blstm_out_paired[:, 1:, :]
    blstm_out_paired = blstm_out_paired[:, :-1, :]

    return blstm_out_paired


def prepare_next_blstm_input(blstm_out, next_conv_out, cfg, idx):

    n_hidden_units = cfg.n_hidden[idx]
    batch_size = tf.shape(blstm_out)[0]

    blstm_out_paired = pair_steps(batch_size, n_hidden_units, blstm_out)

    ni_bias = bias_variable([cfg.n_of_filters[idx + 1]], 'BLSTM-' + str(idx))
    conv_weights = weight_variable([cfg.n_of_filters[idx + 1], cfg.n_of_filters[idx + 1]], 'BLSTM-' + str(idx))

    next_conv_out = tf.reshape(next_conv_out, [-1, cfg.n_of_filters[idx + 1]])
    next_conv_out = tf.matmul(next_conv_out, conv_weights)
    next_conv_out = tf.reshape(next_conv_out, [batch_size, -1, cfg.n_of_filters[idx + 1]])

    next_conv_out = tf.add(blstm_out_paired, next_conv_out)

    next_conv_out = tf.add(next_conv_out, ni_bias)

    return next_conv_out


def logits(cfg, last, in_dimension):
    with tf.variable_scope('Logits'):
        w_log = weight_variable(name='w_log',
                                shape=[in_dimension, cfg.classes])

        b_log = bias_variable(name='b_log',
                              shape=[cfg.classes])

        _logits = tf.add(tf.matmul(last, w_log), b_log, name='logits')

    return _logits, (w_log, b_log)


def weight_variable(shape, name):
    with tf.device('/cpu:0'):
        with tf.variable_scope("Weights_" + name):
            w = tf.get_variable(name, shape=shape, initializer=tf.contrib.layers.xavier_initializer())
    return w


def bias_variable(shape, name):
    with tf.device('/cpu:0'):
        initial = tf.constant(0.1, shape=shape)
        b = tf.Variable(initial, name=name)
    return b


def length(sequence):
    used = tf.sign(tf.reduce_max(tf.abs(sequence), reduction_indices=2))
    _length = tf.reduce_sum(used, reduction_indices=1)
    _length = tf.cast(_length, tf.int32)
    return _length


def _last_relevant(output, _length):
    batch_size = tf.shape(output)[0]
    max_length = tf.shape(output)[1]
    output_size = tf.shape(output)[2]
    index = tf.range(0, batch_size) * max_length + (_length - 1)
    flat = tf.reshape(output, [-1, output_size])
    relevant = tf.gather(flat, index)
    return relevant


def accuracy(labels, _logits, name='accuracy'):
    with tf.variable_scope("my_metrics"):
        acc, a_up = tf.metrics.accuracy(labels=tf.argmax(labels, 1), predictions=tf.argmax(_logits, 1), name=name)
    return acc, a_up


def probabilities(_logits):
    return tf.nn.softmax(_logits, name="softmax_tensor")


def loss(labels, _logits):
    _loss = tf.losses.softmax_cross_entropy(
        onehot_labels=labels, logits=_logits)
    return _loss


def train_op(cfg, _loss):
    optimizer = tf.train.AdamOptimizer(cfg.learning_rate)
    return optimizer.minimize(_loss)


def train_only_on_vars(cfg, _loss, var_list):
    optimizer = tf.train.AdamOptimizer(cfg.learning_rate)
    return optimizer.minimize(_loss, var_list=var_list)


def confusion_mat(labels, _logits):
    return tf.confusion_matrix(tf.argmax(labels, 1), tf.argmax(_logits, 1))


def precision(labels, _logits):
    with tf.variable_scope("my_metrics"):
        precision_stuff = tf.metrics.precision(labels=tf.argmax(labels, 1),
                                               predictions=tf.argmax(_logits, 1))
    return precision_stuff


def recall(labels, _logits):
    with tf.variable_scope("my_metrics"):
        recall_stuff = tf.metrics.recall(labels=tf.argmax(labels, 1),
                                         predictions=tf.argmax(_logits, 1))
    return recall_stuff


def f1_score(precision, recall):
    num = tf.multiply(precision, recall)
    den = tf.add(precision, recall)
    f1 = tf.multiply(2.0, tf.div(num, den))

    return f1


def inc_loss(old_loss, new_loss, count):
    old_count = count - 1
    old_avg = tf.multiply(old_loss, old_count)
    new_avg = tf.add(old_avg, new_loss)
    new_avg = new_avg / count
    return new_avg
