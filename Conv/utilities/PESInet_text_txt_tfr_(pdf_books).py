# PESInet.
#
# Copyright (C) 2017, Davide Francesco Losio, Mauro Luchetti
#
# This file is part of PESInet.
#
#     PESInet is free software: you can redistribute it and/or modify
#     it under the terms of the GNU General Public License as published by
#     the Free Software Foundation, either version 3 of the License, or
#     (at your option) any later version.
#
#     PESInet is distributed in the hope that it will be useful,
#     but WITHOUT ANY WARRANTY; without even the implied warranty of
#     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#     GNU General Public License for more details.
#
#     You should have received a copy of the GNU General Public License
#     along with PESInet.  If not, see <http://www.gnu.org/licenses/>.
# ==========================================================================

import glob
import random

import gensim
import numpy as np
import tensorflow as tf


def write_to_tfrecord(label, binary_text, writer):
    example = tf.train.Example(features=tf.train.Features(feature={
        'labels': tf.train.Feature(bytes_list=tf.train.BytesList(value=[label])),
        'features': tf.train.Feature(bytes_list=tf.train.BytesList(value=[binary_text]))
    }))

    writer.write(example.SerializeToString())


def get_text_bynary(text, label, model):
    counter = 0
    translated_text = []
    lab = []
    notfounded = []

    for word in text:
        if word == 'è':
            word = word.replace('è', "e'")
        counter += 1
        try:
            translated_text.append(model.wv[word])
        except KeyError:
            translated_text.append(model.wv['unknown'])
            notfounded.append(word)

    lab.append(label)
    text = np.array(translated_text, np.float32)
    label = np.array(lab, np.float32)
    return text.tobytes(), label.tobytes(), notfounded, counter


def make_label(file):
    label = file.split('.')[0]
    # 001, questions, 010 exclamations, 100 statements
    if label == 'Questions':
        one_hot_lab = [0, 0, 1]
    elif label == 'Statements':
        one_hot_lab = [1, 0, 0]
    elif label == 'Exclamations':
        one_hot_lab = [0, 1, 0]
    else:
        raise ("the fuck is goin' on")

    return (one_hot_lab, label)


def randomize_and_get(dictionary):
    percentage_of_testset = 0.2     # value between 0 and 1
    num_of_taken_samples = 500000

    testset_dimension = num_of_taken_samples * percentage_of_testset
    dataset_dictionary = {'Questions': [], 'Exclamations': [], 'Sentences': []}
    testset_dictionary = {'Questions': [], 'Exclamations': [], 'Sentences': []}

    for key, value in dictionary.items():

        print('Shuffling ', key)
        random.shuffle(value)

        print('Creating ' + key + 'dataset and testset')
        testset_dictionary[key] = value[:testset_dimension]
        dataset_dictionary[key] = value[testset_dimension:]

    return dataset_dictionary, testset_dictionary


def prepare_tfr(dictionary, tfrname):
    counter = 0
    notf = []

    writer = tf.python_io.TFRecordWriter(tfrname)

    for key, value in dictionary.items():
        for sample in value:
            text = sample.split()
            text_byn, oh_lab_byn, nf, cnt = get_text_bynary(text, one_hot_lab, model)
            counter += cnt
            if nf:
                notf.append(nf)
            if len(text_byn) != 0:
                write_to_tfrecord(oh_lab_byn, text_byn, writer)

    writer.close()

    print("tot word:", counter)
    print("words not found:", len(notf))
    print(notf)


if __name__ == '__main__':

    path_to_the_three_txt_files = '/*.txt'
    files_dictionary = {'Questions': [], 'Exclamations': [], 'Sentences': []}

    model = gensim.models.Word2Vec.load('modelsw2v/glove_WIKI')
    files = glob.glob(path_to_the_three_txt_files)

    # iterate over the list getting each file

    for file in files:
        # open the file and then call .read() to get the text
        with open(file, encoding='utf-8') as f:
            (one_hot_lab, label) = make_label(file)
            print('procesing file name:', file, 'Label:', label, 'One Hot Repr:', one_hot_lab)
            text = f.read().splitlines()
            for line in text:
                files_dictionary[label] += line

    dataset, testset = randomize_and_get(files_dictionary)
    prepare_tfr(dataset, 'Smoothed.tfrecords')
    prepare_tfr(testset, 'test0.tfrecords')
