# PESInet.
#
# Copyright (C) 2017, Davide Francesco Losio, Mauro Luchetti
#
# This file is part of PESInet.
#
#     PESInet is free software: you can redistribute it and/or modify
#     it under the terms of the GNU General Public License as published by
#     the Free Software Foundation, either version 3 of the License, or
#     (at your option) any later version.
#
#     PESInet is distributed in the hope that it will be useful,
#     but WITHOUT ANY WARRANTY; without even the implied warranty of
#     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#     GNU General Public License for more details.
#
#     You should have received a copy of the GNU General Public License
#     along with PESInet.  If not, see <http://www.gnu.org/licenses/>.
# ==========================================================================

import os

if __name__ == '__main__':

    local_path = os.path.dirname(__file__) + '/'
    audio_script = 'PESInet_sound_csv_tfr.py'
    text_script = 'PESInet_text_txt_tfr.py'

    audio_path = '/Volumes/SAMSUNG/DATASET/sound_and_text_dataset/Audio/'
    text_path = '/Volumes/SAMSUNG/DATASET/sound_and_text_dataset/Text/'

    statements_path = 'Affermations/'
    exclamations_path = 'Exclamations/'
    questions_path = 'Questions/'

    audio_output_file_name = 'audio.tfrecords'
    text_output_file_name = 'text.tfrecords'

    print('Creating audio dataset...')
    os.system('Python ' + local_path + audio_script + ' ' +
              audio_path + statements_path + ' ' +
              audio_path + exclamations_path + ' ' +
              audio_path + questions_path + ' ' +
              audio_path + audio_output_file_name)

    print('Creating text dataset...')
    os.system('Python ' + local_path + text_script + ' ' +
              text_path + statements_path + ' ' +
              text_path + exclamations_path + ' ' +
              text_path + questions_path + ' ' +
              text_path + text_output_file_name)
