# PESInet.
#
# Copyright (C) 2017, Davide Francesco Losio, Mauro Luchetti
#
# This file is part of PESInet.
#
#     PESInet is free software: you can redistribute it and/or modify
#     it under the terms of the GNU General Public License as published by
#     the Free Software Foundation, either version 3 of the License, or
#     (at your option) any later version.
#
#     PESInet is distributed in the hope that it will be useful,
#     but WITHOUT ANY WARRANTY; without even the implied warranty of
#     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#     GNU General Public License for more details.
#
#     You should have received a copy of the GNU General Public License
#     along with PESInet.  If not, see <http://www.gnu.org/licenses/>.
# ==========================================================================

import glob
import os
import random
import shutil
import sys

q_path = '/Volumes/SAMSUNG/DATASET/root_path_for_doubled_dataset/out/audio/Questions/*.csv'
e_path = '/Volumes/SAMSUNG/DATASET/root_path_for_doubled_dataset/out/audio/Exclamations/*.csv'
s_path = '/Volumes/SAMSUNG/DATASET/root_path_for_doubled_dataset/out/audio/Statements/*.csv'
dest_path = '/Volumes/SAMSUNG/DATASET/root_path_for_doubled_dataset/out/audio/TestSet/'


def take_20x_samples(path):
    files = glob.glob(path)

    lengz = len(files)

    n_of_elems = int(lengz * 0.1)
    print('# of moved elements:', n_of_elems)

    random.seed(a=7)
    choosen_elem = random.sample(population=files, k=n_of_elems)

    for elem in choosen_elem:
        print('moving elem:', elem)
        elem_clean_path = elem.replace('\\', '/')
        (head, filename) = os.path.split(elem_clean_path)
        (_, label) = os.path.split(head)
        shutil.move(elem_clean_path, dest_path + '/' + label + '/' + filename)


if __name__ == '__main__':

    # take a list of folder path as argument, otherwise use the default one
    if len(sys.argv) > 1:
        paths = sys.argv[1:]
    else:
        paths = [q_path, s_path, e_path]

    for path in paths:
        print('Moving files in:', path)
        take_20x_samples(path)
