import argparse
import glob
import os

from selenium import webdriver
from selenium.common.exceptions import *
from selenium.webdriver.support import expected_conditions as ec
from selenium.webdriver.support.ui import WebDriverWait

# parser init for main arguments
parser = argparse.ArgumentParser(description='Parser')

# parser parameters
parser.add_argument('--driver_path',
                    type=str,
                    default='chrome_driver/chromedriver',
                    help='specify the chrome driver path')

parser.add_argument('--username',
                    type=str,
                    default='rtedesco',
                    help='username')

parser.add_argument('--passw',
                    type=str,
                    default='rtedesco',
                    help='login password')

parser.add_argument('--chapters_folder',
                    type=str,
                    default='C:\\Users\\david\\PycharmProjects\\tenzor-flow-nlp-2\\utilities\\Chap_to_upload',
                    help='login password')

# net Parameters
DEFAULT_SLEEP_TIME = 100  # max waiting for the loading of a page element
LOG_URL = "http://rs.voisis.it/rs-service/index.php"  # login page
UP_PAGE = "http://rs.voisis.it/rs-service/Upload/upload.php"  # upload files page

PAGE_COMPONENTS_ID = {
    'login': "//table[@id='login-pres']",
    'user': "//input[@name='rs_username']",
    'pass': "//input[@name='rs_password']",
    'log_btn': "//input[@class='button red']",
    'cont_btn': "//input[@id='skipLic']",
    'add_files': "//input[@name='files[]']",
    'up_btns': "//button[@class='btn red']"
}


# Helper Methods

def extract_element(parent, id_type, identifier, num=0):
    if id_type == 'xpath':
        element = parent.find_elements_by_xpath(identifier)[num]
    else:
        raise FileNotFoundError('%s type unhandled' % (id_type))
    return element


def get_browser(driver_path=""):
    try:
        brow = webdriver.Chrome(executable_path=driver_path)
    except WebDriverException:
        print("ChromeDriver not found, trying with firefox")
        brow = webdriver.Firefox()
    return brow


def get_url(brow, url):
    brow.get(url)
    wait_element(brow, 'xpath', PAGE_COMPONENTS_ID['login'], 'on_vis')


def wait_element(brow, id_type, element_id, wait_type='on_vis', timeout=DEFAULT_SLEEP_TIME):
    if wait_type == 'on_vis':
        element = ec.visibility_of_element_located((id_type, element_id))
    elif wait_type == 'on_presence':
        element = ec.presence_of_element_located((id_type, element_id))
    else:
        raise Exception("wait functionality not supported.")

    try:
        return WebDriverWait(brow, timeout).until(element)
    except TimeoutException:
        print("Timed out waiting for page to load")
        brow.quit()
        raise TimeoutException


def click(brow, id_type, identifier):
    wait_element(brow, id_type, identifier, 'on_vis')
    element = extract_element(brow, id_type, identifier)
    element.click()


def type_key(brow, id_type, identifier, keys):
    wait_element(brow, id_type, identifier, 'on_vis')
    element = extract_element(brow, id_type, identifier)
    element.send_keys(keys)


def log_in(brow, credential):
    click(brow, 'xpath', PAGE_COMPONENTS_ID['user'])
    type_key(brow, 'xpath', PAGE_COMPONENTS_ID['user'], credential['username'])
    click(brow, 'xpath', PAGE_COMPONENTS_ID['pass'])
    type_key(brow, 'xpath', PAGE_COMPONENTS_ID['pass'], credential['passw'])
    click(brow, 'xpath', PAGE_COMPONENTS_ID['log_btn'])
    click(brow, 'xpath', PAGE_COMPONENTS_ID['cont_btn'])
    brow.get(UP_PAGE)


def up_files(brow, files):
    elem = wait_element(brow, 'xpath', PAGE_COMPONENTS_ID['add_files'], 'on_presence')
    filenames_list = []
    for file in files:
        filenames_list.append(os.path.basename(file))
        elem.send_keys(file)
        elem = wait_element(brow, 'xpath', PAGE_COMPONENTS_ID['add_files'], 'on_presence')

    buttons = brow.find_elements_by_xpath(PAGE_COMPONENTS_ID['up_btns'])

    for button in buttons:
        button.click()
        identifier = ".//p[descendant::span[contains(text(), '%s')] and descendant::span[contains(text(), '...trasferito')]]" % (
        filenames_list[0])
        wait_element(brow, 'xpath', identifier)
        filenames_list = filenames_list[1:]


# !! MAIN - DANGER, handle with EXTREME care !!
if __name__ == '__main__':

    args = parser.parse_args()

    files_to_upload = glob.glob(args.chapters_folder + "\\*.mp3")
    for i in range(len(files_to_upload)):
        files_to_upload.append(files_to_upload[0].replace("\\", "\\\\"))
        files_to_upload = files_to_upload[1:]
        print("file found: %s, \n" % (files_to_upload[-1]))

    # open-up a browser and navigate to TIGROS home-page
    browser = get_browser(args.driver_path)
    get_url(browser, LOG_URL)

    credential = {
        'username': args.username,
        'passw': args.passw
    }

    log_in(browser, credential)
    up_files(browser, files_to_upload)

    # browser.quit()
