# PESInet.
#
# Copyright (C) 2017, Davide Francesco Losio, Mauro Luchetti
#
# This file is part of PESInet.
#
#     PESInet is free software: you can redistribute it and/or modify
#     it under the terms of the GNU General Public License as published by
#     the Free Software Foundation, either version 3 of the License, or
#     (at your option) any later version.
#
#     PESInet is distributed in the hope that it will be useful,
#     but WITHOUT ANY WARRANTY; without even the implied warranty of
#     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#     GNU General Public License for more details.
#
#     You should have received a copy of the GNU General Public License
#     along with PESInet.  If not, see <http://www.gnu.org/licenses/>.
# ==========================================================================

import tensorflow as tf


def variable_summaries(var, name):
    'Attach a lot of summaries to a Tensor (for TensorBoard visualization).'

    with tf.name_scope(name + 'summaries'):
        mean = tf.reduce_mean(var)
        tf.summary.scalar('mean', mean)
        with tf.name_scope('stddev'):
            stddev = tf.sqrt(tf.reduce_mean(tf.square(var - mean)))
        tf.summary.scalar('stddev', stddev)
        tf.summary.scalar('max', tf.reduce_max(var))
        tf.summary.scalar('min', tf.reduce_min(var))
        tf.summary.histogram('histogram', var)


def histogram(var, name):
    return tf.summary.histogram(name, var)


def scalar(var, name):
    return tf.summary.scalar(name, var)


def tenzor(tensor, name):
    return tf.summary.tensor_summary(name, tensor)


def write_on_tensorboard(summary, writer, step):
    writer.add_summary(summary, step)
    writer.flush()


def instantiate_writers(cfg, graph, stats_sets):
    writers = {}
    for name in stats_sets:
        writers[name] = tf.summary.FileWriter(cfg.train_logs + name, graph)

    return writers


def add_metrics(metrics):
    metrics_ids = []
    for name, metric in metrics.items():
        metrics_ids.append(scalar(metric, name))

    return metrics_ids
