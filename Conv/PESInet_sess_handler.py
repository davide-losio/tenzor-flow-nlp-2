# PESInet.
#
# Copyright (C) 2017, Davide Francesco Losio, Mauro Luchetti
#
# This file is part of PESInet.
#
#     PESInet is free software: you can redistribute it and/or modify
#     it under the terms of the GNU General Public License as published by
#     the Free Software Foundation, either version 3 of the License, or
#     (at your option) any later version.
#
#     PESInet is distributed in the hope that it will be useful,
#     but WITHOUT ANY WARRANTY; without even the implied warranty of
#     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#     GNU General Public License for more details.
#
#     You should have received a copy of the GNU General Public License
#     along with PESInet.  If not, see <http://www.gnu.org/licenses/>.
# ==========================================================================

import tensorflow as tf


class SessHandler(object):

    def __init__(self, config, ps, ts):
        self.cfg = config
        self.cv_counter = 0
        self.k = 0
        self.ps = ps
        self.ts = ts

    def cross_validation_session(self):
        self.k = self.cfg.k
        while self.k != 0:
            tf.logging.info(' starting %i cross validation session' % self.cv_counter)
            self.init_session()
        tf.logging.info(' End of %i-fold cross validation')

    def train_session(self):
        self.init_session()
        tf.logging.info(' End of training session')

    def prediction_session(self):
        tf.logging.info('starting prediction session')
        pred_sess = self.ps.PredSess(self.cfg)
        model = pred_sess.init_sess()
        return pred_sess.predict(self.cfg.ckpt_path, model)

    def init_session(self):

        # train for some epochs
        train_session_ = self.ts.TrainSess(self.cfg, self.cv_counter)
        train_session_.init_datasets()
        train_session_.launch_training_session()

        # memory cleaning ops
        train_session_ = None
        del train_session_

        # next session operations
        self.k -= 1
        self.cv_counter += 1
