# PESInet.
#
# Copyright (C) 2017, Davide Francesco Losio, Mauro Luchetti
#
# This file is part of PESInet.
#
#     PESInet is free software: you can redistribute it and/or modify
#     it under the terms of the GNU General Public License as published by
#     the Free Software Foundation, either version 3 of the License, or
#     (at your option) any later version.
#
#     PESInet is distributed in the hope that it will be useful,
#     but WITHOUT ANY WARRANTY; without even the implied warranty of
#     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#     GNU General Public License for more details.
#
#     You should have received a copy of the GNU General Public License
#     along with PESInet.  If not, see <http://www.gnu.org/licenses/>.
# ==========================================================================

import PESInet_Model.PESInet_model as mod

import PESInet_predict_sess as pps


class PredSess(pps.PredictSess):

    def __init__(self, cfg):
        super(PredSess, self).__init__(cfg)

    def init_sess(self):
        model = {}

        path = self.cfg.pred_path
        inputs = self.get_inputset(cfg=self.cfg, name='Datasets_init', path=path)

        model['iterator'] = inputs.make_one_shot_iterator()
        batchf, batchl = model['iterator'].get_next()
        model['batchf'] = batchf
        model['batchl'] = batchl
        h_feat = self.get_h_feat(batchf, self.cfg, self.cfg.mode)
        logits, _ = self.get_logits(h_feat, self.cfg.n_hidden[-1] * 2, self.cfg, self.cfg.mode)
        model['logits'] = logits
        model['softm'] = mod.probabilities(model['logits'][0])

        return model
