# mod.
#
# Copyright (C) 2017, Davide Francesco Losio, Mauro Luchetti
#
# This file is part of mod.
#
#     mod is free software: you can redistribute it and/or modify
#     it under the terms of the GNU General Public License as published by
#     the Free Software Foundation, either version 3 of the License, or
#     (at your option) any later version.
#
#     mod is distributed in the hope that it will be useful,
#     but WITHOUT ANY WARRANTY; without even the implied warranty of
#     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#     GNU General Public License for more details.
#
#     You should have received a copy of the GNU General Public License
#     along with mod.  If not, see <http://www.gnu.org/licenses/>.
# ==========================================================================

import PESInet_train_sess as Pts
import tensorflow as tf

import PESInet_Model.PESInet_model as Mod


class TrainSess(Pts.TrainSess):
    def __init__(self, cfg, cv_counter):
        super(TrainSess, self).__init__(cfg, cv_counter)

    def init_datasets(self):
        sets = dict()

        sets['data_set'] = self.get_data_set(name='Dataset_init',
                                             cfg=self.cfg,
                                             _type='data',
                                             path=self.cfg.train_path)

        sets['validation_set'] = self.get_data_set(name='Validation_set',
                                                   cfg=self.cfg,
                                                   _type='valid',
                                                   path=self.cfg.validation_path)

        sets['canary_set'] = sets['data_set']

        self.iter_handles, self.next_element = self.get_iterator(sets)

    def launch_training_session(self):
        batch_f, self.batch_l = self.next_element
        h_feat = self.get_h_feat(batch_f, self.cfg, self.cfg.mode)
        logits, _ = self.get_logits(h_feat, self.cfg.n_hidden[-1] * 2, self.cfg, self.cfg.mode)
        pred = Mod.probabilities(logits)

        self.setup_train_params(pred=pred, batch_l=self.batch_l)

        self.training()
        self.close()

    def training(self):
        training_feeds = {self.iter_handles['handle']: self.iter_handles['data_set_handle'],
                          self.keep_prob: 0.95}
        stuff_to_run = [self.train_op]
        self.training_cycle(training_feeds, stuff_to_run)

    def validating(self):

        validating_feeds = {self.iter_handles['handle']: self.iter_handles['validation_set_handle'],
                            self.keep_prob: 1.0,
                            self.old_loss: 0.0,
                            self.count: 0.0
                            }

        canary_feeds = {self.iter_handles['handle']: self.iter_handles['canary_set_handle'],
                        self.keep_prob: 1.0}

        self.validating_cycle(validating_feeds, canary_feeds)

    def close(self):
        tf.reset_default_graph()
        tf.contrib.keras.backend.clear_session()
        self.train_sess = None
        del self.train_sess
        self.iter_handles = None
        del self.iter_handles
