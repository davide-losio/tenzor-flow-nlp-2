# PESInet.
#
# Copyright (C) 2017, Davide Francesco Losio, Mauro Luchetti
#
# This file is part of PESInet.
#
#     PESInet is free software: you can redistribute it and/or modify
#     it under the terms of the GNU General Public License as published by
#     the Free Software Foundation, either version 3 of the License, or
#     (at your option) any later version.
#
#     PESInet is distributed in the hope that it will be useful,
#     but WITHOUT ANY WARRANTY; without even the implied warranty of
#     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#     GNU General Public License for more details.
#
#     You should have received a copy of the GNU General Public License
#     along with PESInet.  If not, see <http://www.gnu.org/licenses/>.
# ==========================================================================

from __future__ import absolute_import
from __future__ import division
from __future__ import print_function

import argparse as argparse

import PESInet_sess_handler as Sh
import tensorflow as tf
from configurations import PESInet_config_handler as Pch
from txt_AND_snd_sess import PESInet_and_predict as Aps, PESInet_bayesian_prediction as Pbp
from txt_OR_snd_sess import PESInet_or_predict as Ps, PESInet_or_train as Ts

from txt_AND_snd_sess import PESInet_and_train as Ats

tf.logging.set_verbosity(tf.logging.INFO)

if __name__ == '__main__':

    config = {}
    parser = argparse.ArgumentParser()
    ps = []
    ts = []

    parser.add_argument("--features_type", default="txt",
                        help="select -txt- for text features, -snd- for sound features,"
                             + " st for sound and text features together"
                             + " stp for sound and text probabilistic")

    parser.add_argument("--mode", default="TR",
                        help="select CV for cross validation, TR for train, P"
                             + " for prediction")

    FLAGS, _ = parser.parse_known_args()

    # set config

    if FLAGS.features_type == 'snd':
        config = Pch.get_sound_cfg()
        ps = Ps
        ts = Ts

    elif FLAGS.features_type == 'txt':
        config = Pch.get_text_cfg()
        ps = Ps
        ts = Ts

    elif FLAGS.features_type == 'st':
        config = Pch.get_combined_cfg()
        ps = Aps
        ts = Ats

    elif FLAGS.features_type == 'stp':
        config = Pch.get_combined_cfg()
        ps = Pbp
        ts = Pbp

    # set operation mode

    if FLAGS.mode == 'CV':
        # TODO implement cross validation mode also for sound and text net if needed.
        if FLAGS.features_type == 'st':
            raise NotImplementedError
        cross_validation_session = Sh.SessHandler(config, ps, ts)
        cross_validation_session.cross_validation_session()
        tf.logging.info(' End of the cross validation.')

    elif FLAGS.mode == 'TR':
        training_session = Sh.SessHandler(config, ps, ts)
        training_session.train_session()
        tf.logging.info(' End of training session')

    elif FLAGS.mode == 'P':
        pred_session = Sh.SessHandler(config, ps, ts)
        pred_session.prediction_session()
