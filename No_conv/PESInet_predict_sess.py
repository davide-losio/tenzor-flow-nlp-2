# PESInet.
#
# Copyright (C) 2017, Davide Francesco Losio, Mauro Luchetti
#
# This file is part of PESInet.
#
#     PESInet is free software: you can redistribute it and/or modify
#     it under the terms of the GNU General Public License as published by
#     the Free Software Foundation, either version 3 of the License, or
#     (at your option) any later version.
#
#     PESInet is distributed in the hope that it will be useful,
#     but WITHOUT ANY WARRANTY; without even the implied warranty of
#     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#     GNU General Public License for more details.
#
#     You should have received a copy of the GNU General Public License
#     along with PESInet.  If not, see <http://www.gnu.org/licenses/>.
# ==========================================================================

import PESInet_Model.PESInet_model_no_conv as Mod
import tensorflow as tf

import PESInet_Model.PESInet_dataset as Dt


class PredictSess(object):
    def __init__(self, config):
        self.cfg = config
        self.keep_prob = 1.0

    def init_sess(self):
        pass

    @staticmethod
    def get_inputset(cfg, name, path='./Smoothed.tfrecords'):

        inputs = None

        with tf.name_scope(name):
            try:
                inputs = Dt.Dataset(cfg, path).get_dataset()
            except FileNotFoundError:
                tf.logging.info('inputset path is wrong, change them dickhead!')
                raise FileNotFoundError

        return inputs

    @staticmethod
    def predict(ck_path, model):

        predicted_labs = []
        logit = model['logits']

        # prediction session
        with tf.Session() as sess:
            sess.run(tf.global_variables_initializer())
            saver = tf.train.Saver()
            # Restore variables from disk.
            saver.restore(sess, ck_path)
            tf.logging.info("PESInet_Model restored.")
            while True:
                try:
                    tmp_pred = sess.run([tf.argmax(logit, 1)])
                    predicted_labs += [tmp_pred]
                except tf.errors.OutOfRangeError:
                    break

        total_count = 0

        for batch_i in range(len(predicted_labs)):
            batch_predicted_labs = predicted_labs[batch_i]
            for i in range(len(batch_predicted_labs)):
                total_count += 1
                tf.logging.info("pred: %i" % (batch_predicted_labs[i]))

        return

    @staticmethod
    def get_logits(h_feat, in_dimension, cfg, name='logits'):
        with tf.variable_scope(name):
            logits, (w_log, b_log) = Mod.logits(cfg=cfg, last=h_feat, in_dimension=in_dimension)
        return logits, (w_log, b_log)

    @staticmethod
    def get_prob(_logits, name='softmax'):
        with tf.variable_scope(name):
            probs = Mod.probabilities(_logits)
        return probs

    @staticmethod
    def restore(sess, ckpt_path, mode):
        var_list = [v for v in tf.get_collection(tf.GraphKeys.GLOBAL_VARIABLES) if mode in v.name]
        saver = tf.train.Saver(var_list)
        saver.restore(sess, ckpt_path)

    def get_h_feat(self, batchf, cfg, name):

        with tf.variable_scope(name):
            h_feat = Mod.high_level_feat(cfg=cfg, features=batchf, keep_prob=self.keep_prob)
        return h_feat
