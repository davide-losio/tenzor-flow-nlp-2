# PESInet.
#
# Copyright (C) 2017, Davide Francesco Losio, Mauro Luchetti
#
# This file is part of PESInet.
#
#     PESInet is free software: you can redistribute it and/or modify
#     it under the terms of the GNU General Public License as published by
#     the Free Software Foundation, either version 3 of the License, or
#     (at your option) any later version.
#
#     PESInet is distributed in the hope that it will be useful,
#     but WITHOUT ANY WARRANTY; without even the implied warranty of
#     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#     GNU General Public License for more details.
#
#     You should have received a copy of the GNU General Public License
#     along with PESInet.  If not, see <http://www.gnu.org/licenses/>.
# ==========================================================================

import tensorflow as tf

import PESInet_train_sess as Pts


class TrainSess(Pts.TrainSess):
    def __init__(self, cfg, cv_counter):
        super(TrainSess, self).__init__(cfg, cv_counter)
        self.txt_cfg = self.cfg.txt_cfg
        self.snd_cfg = self.cfg.snd_cfg

    def init_datasets(self):
        snd_set = self.get_data_set(name='snd_net',
                                    _type='data',
                                    cfg=self.snd_cfg,
                                    path=self.snd_cfg.train_path)

        txt_set = self.get_data_set(name='txt_net',
                                    _type='data',
                                    cfg=self.txt_cfg,
                                    path=self.txt_cfg.train_path)

        valid_snd_set = self.get_data_set(name='snd_valid_set',
                                          _type='valid',
                                          cfg=self.snd_cfg,
                                          path=self.snd_cfg.validation_path)

        valid_txt_set = self.get_data_set(name='txt_valid_set',
                                          _type='valid',
                                          cfg=self.txt_cfg,
                                          path=self.txt_cfg.validation_path)

        snd_sets = dict()
        snd_sets['snd_set'] = snd_set
        snd_sets['c_snd_set'] = snd_set
        snd_sets['vld_snd_set'] = valid_snd_set

        txt_sets = dict()
        txt_sets['txt_set'] = txt_set
        txt_sets['c_txt_set'] = txt_set
        txt_sets['vld_txt_set'] = valid_txt_set

        self.iter_handles, self.next_element = self.init_iter(txt_sets, snd_sets)

    def launch_training_session(self):
        t_batch_f, t_batch_l = self.next_element['t_next_element']
        s_batch_f, s_batch_l = self.next_element['s_next_element']
        txt_h_feat = self.get_h_feat(t_batch_f, self.cfg.txt_cfg, self.cfg.txt_cfg.mode)
        snd_h_feat = self.get_h_feat(s_batch_f, self.cfg.snd_cfg, self.cfg.snd_cfg.mode)

        h_feat = tf.concat([txt_h_feat, snd_h_feat], 1)

        h_feat = tf.nn.dropout(h_feat, self.keep_prob)

        in_dimension = (self.cfg.txt_cfg.n_hidden[-1] + self.cfg.snd_cfg.n_hidden[-1]) * 2

        logits, _ = self.get_logits(h_feat, in_dimension, self.cfg, 'fully_connected_layer')

        self.setup_train_params(pred=logits, batch_l=t_batch_l)

        self.restore(self.txt_cfg.ckpt_path, self.cfg.txt_cfg.mode)
        self.restore(self.snd_cfg.ckpt_path, self.cfg.snd_cfg.mode)

        self.training()

        self.close()

    def init_iter(self, txt_sets, snd_sets):
        t_handles_stuff, t_next_element = self.get_iterator(txt_sets)
        s_handles_stuff, s_next_element = self.get_iterator(snd_sets)

        next_element = {
            't_next_element': t_next_element,
            's_next_element': s_next_element
        }

        iter_handles = {
            'txt': t_handles_stuff,
            'snd': s_handles_stuff
        }

        return iter_handles, next_element

    def training(self):
        training_feeds = {self.iter_handles['txt']['handle']: self.iter_handles['txt']['txt_set_handle'],
                          self.iter_handles['snd']['handle']: self.iter_handles['snd']['snd_set_handle'],
                          self.keep_prob: 0.8}
        stuff_to_run = [self.train_op, self.merge_train_vars]
        self.training_cycle(training_feeds, stuff_to_run)

    def validating(self):
        validating_feeds = {self.iter_handles['txt']['handle']: self.iter_handles['txt']['vld_txt_set_handle'],
                            self.iter_handles['snd']['handle']: self.iter_handles['snd']['vld_snd_set_handle'],
                            self.keep_prob: 1.0,
                            self.old_loss: 0.0,
                            self.count: 0.0
                            }

        canary_feeds = {self.iter_handles['txt']['handle']: self.iter_handles['txt']['c_txt_set_handle'],
                        self.iter_handles['snd']['handle']: self.iter_handles['snd']['c_snd_set_handle'],
                        self.keep_prob: 1.0}

        self.validating_cycle(validating_feeds, canary_feeds)
