# PESInet.
#
# Copyright (C) 2017, Davide Francesco Losio, Mauro Luchetti
#
# This file is part of PESInet.
#
#     PESInet is free software: you can redistribute it and/or modify
#     it under the terms of the GNU General Public License as published by
#     the Free Software Foundation, either version 3 of the License, or
#     (at your option) any later version.
#
#     PESInet is distributed in the hope that it will be useful,
#     but WITHOUT ANY WARRANTY; without even the implied warranty of
#     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#     GNU General Public License for more details.
#
#     You should have received a copy of the GNU General Public License
#     along with PESInet.  If not, see <http://www.gnu.org/licenses/>.
# ==========================================================================

import PESInet_Model.PESInet_model_no_conv as Mod
import tensorflow as tf
import utilities.PESInet_tensorboard_utilities as Tu
import PESInet_Model.PESInet_dataset as Dt
import math


class TrainSess(object):
    def __init__(self, cfg, cv_counter):

        self.cv_counter = cv_counter
        self.cfg = cfg
        config = tf.ConfigProto()
        config.gpu_options.allow_growth = True
        self.train_sess = tf.Session(config=config)
        self.iter_handles = {}
        self.next_element = {}
        self.train_step = 0
        self.stop_me = 5
        self.train_op = {}
        self.accuracy = {}
        self.confusion_mat = {}
        self.writers = {}
        self.keep_prob = tf.placeholder(tf.float32)
        self.old_loss = tf.placeholder(tf.float32)
        self.count = tf.placeholder(tf.float32)
        self.precision = {}
        self.recall = {}
        self.r_up = {}
        self.a_up = {}
        self.p_up = {}
        self.metrics_init = {}
        self.loss_measure = {}
        self.merge_train_vars = {}
        self.merge_metrics = {}
        self.f1_score = {}
        self.merge_loss = {}

    def get_iterator(self, sets):

        handles_stuff = {}

        with tf.device('/cpu:0'):
            with tf.name_scope('Iterators_init'):
                handles_stuff['handle'] = tf.placeholder(tf.string, shape=[])
                iterator = tf.contrib.data.Iterator.from_string_handle(
                    handles_stuff['handle'],
                    list(sets.values())[0].output_types,
                    list(sets.values())[0].output_shapes)
                next_element = iterator.get_next()
                for name, _set in sets.items():
                    set_iterator = _set.make_one_shot_iterator()
                    handles_stuff[name + '_handle'] = self.train_sess.run(set_iterator.string_handle())

        return handles_stuff, next_element

    def get_data_set(self, cfg, name, _type, path='./0.tfrecords'):

        with tf.name_scope(name):
            try:
                if _type == 'data':
                    _set = Dt.Dataset(cfg, path).get_epoch_dataset()
                elif _type == 'valid':
                    _set = Dt.Validationset(cfg, path).get_validationset()
                else:
                    raise NotImplementedError
            except FileNotFoundError:
                print('%s path is wrong, change them dickhead!' % name)
                raise FileNotFoundError

            return _set

    def early_stopping(self, loss, params_to_save):
        if math.isnan(loss):
            loss = self.stop_me - 0.1
        if self.stop_me > loss and self.cfg.save == 'True':
            tf.logging.info('Saving checkpoint in path {}...'.format(self.cfg.ckpt_path))
            self.stop_me = loss
            saver = tf.train.Saver()
            saver.save(self.train_sess, self.cfg.ckpt_path)
            tf.logging.info('Checkpoint saved')
            self.print_on_txt(params_to_save)
            return
        else:
            return

    def print_on_txt(self, to_print):
        with open(str(self.cfg.mode) + "_best_params.txt", "w") as text:
            for key, value in sorted(to_print.items()):
                if 'conf' in key:
                    text.write(' %s:\n%s \n' % (key, value))
                else:
                    text.write(' %s: %s \n' % (key, value))

    def setup_train_params(self, pred, batch_l, vars_to_train='all'):

        # set up training op and parameters

        self.writers = Tu.instantiate_writers(self.cfg, self.train_sess.graph, ['train', 'valid'])
        train_loss = Mod.loss(labels=batch_l, _logits=pred)
        self.train_op = self.get_tr_op(train_loss, vars_to_train)
        self.accuracy, self.a_up = Mod.accuracy(labels=batch_l, _logits=pred)
        self.confusion_mat = Mod.confusion_mat(labels=batch_l, _logits=pred)
        self.precision, self.p_up = Mod.precision(labels=batch_l, _logits=pred)
        self.recall, self.r_up = Mod.recall(labels=batch_l, _logits=pred)
        self.f1_score = Mod.f1_score(self.precision, self.recall)
        self.loss_measure = Mod.inc_loss(self.old_loss, train_loss, self.count)

        self.merge_loss = tf.summary.merge([Tu.scalar(self.loss_measure, 'loss')])
        self.merge_metrics = tf.summary.merge(Tu.add_metrics({
            'accuracy': self.accuracy,
            'f1_score': self.f1_score
        }))

        metrics_stats = [v for v in tf.get_collection(tf.GraphKeys.LOCAL_VARIABLES) if "my_metrics" in v.name]
        self.metrics_init = tf.variables_initializer(var_list=metrics_stats)

        self.train_sess.run(tf.global_variables_initializer())

    @staticmethod
    def print_res(to_print):

        for key, value in sorted(to_print.items()):
            if 'conf' in key:
                tf.logging.info(' %s:\n%s' % (key, value))
            else:
                tf.logging.info(' %s: %s' % (key, value))

    def get_tr_op(self, _loss, vars_to_train):

        if vars_to_train == 'all':
            train_op = Mod.train_op(cfg=self.cfg, _loss=_loss)
        elif vars_to_train:
            train_op = Mod.train_only_on_vars(cfg=self.cfg, _loss=_loss, var_list=vars_to_train)
        else:
            raise NotImplementedError

        return train_op

    def training_cycle(self, training_feeds, stuff_to_run):
        while True:
            self.train_step += 1
            try:
                self.train_sess.run(stuff_to_run, feed_dict=training_feeds)
                if (self.cfg.batch_size * self.train_step) % self.cfg.dataset_size == 0:
                    epoch = str((self.cfg.batch_size * self.train_step) / self.cfg.dataset_size)
                    tf.logging.info('\\/\\/\\/\\/\\/\\/\\/\\/\\/ epoch nr: ' + epoch + ' /\\/\\/\\/\\/\\/\\/\\/\\/\\/')
                    self.validating()
                    tf.logging.info('/\\/\\/\\/\\/\\/\\/\\/\\/\\/\\/\\/\\/\\/\\/\\/\\/\\/\\/\\/\\/\\/\\/\\/\\/\\/\\/\n')
            except tf.errors.OutOfRangeError:
                break

    def validating_cycle(self, validating_feeds, canary_feeds):

        metrics_to_update = [self.a_up, self.r_up, self.p_up, self.confusion_mat, self.loss_measure, self.merge_loss]
        metrics_to_calculate = [self.merge_metrics, self.accuracy, self.recall, self.precision, self.f1_score]

        confusion_matrixes = []
        writer = self.writers['valid']
        summary_l = {}
        count = 0.0
        old_loss = 0.0

        self.train_sess.run(self.metrics_init)

        while count * self.cfg.validation_batch != self.cfg.validation_size:
            count += 1.0
            validating_feeds[self.count] = count
            _, _, _, confusion_matrix, old_loss, summary_l = self.train_sess.run(metrics_to_update,
                                                                                 validating_feeds)
            validating_feeds[self.old_loss] = old_loss
            confusion_matrixes.append(confusion_matrix)

        summary_m, accuracy, recall, precision, f1_score = self.train_sess.run(metrics_to_calculate)
        confusion_matrix = self.sum_metrics(confusion_matrixes)

        to_print = dict()

        to_print['val_acc'] = accuracy
        to_print['f1_score'] = f1_score
        to_print['confusion_matrix'] = confusion_matrix
        to_print['loss'] = old_loss

        Tu.write_on_tensorboard(summary=summary_m, writer=writer, step=self.train_step)
        Tu.write_on_tensorboard(summary=summary_l, writer=self.writers['train'], step=self.train_step)

        self.train_sess.run(self.metrics_init)
        self.train_sess.run(self.a_up, feed_dict=canary_feeds)
        to_print['canary_accuracy'] = self.train_sess.run(self.accuracy)

        self.print_res(to_print)
        self.early_stopping(old_loss, to_print)

    @staticmethod
    def sum_metrics(metrics):
        metric = metrics[0]
        for i in range(len(metrics) - 1):
            metric = metric + metrics[i + 1]
        return metric

    @staticmethod
    def get_logits(h_feat, in_dimension, cfg, name='logits'):

        with tf.variable_scope(name):
            logits, (w_log, b_log) = Mod.logits(cfg=cfg, last=h_feat, in_dimension=in_dimension)
        return logits, (w_log, b_log)

    def get_h_feat(self, batchf, cfg, name):

        with tf.variable_scope(name):
            h_feat = Mod.high_level_feat(cfg=cfg, features=batchf, keep_prob=self.keep_prob)
        return h_feat

    def restore(self, ckpt_path, mode):
        var_list = [v for v in tf.get_collection(tf.GraphKeys.GLOBAL_VARIABLES) if mode in v.name]
        saver = tf.train.Saver(var_list)
        saver.restore(self.train_sess, ckpt_path)
        tf.logging.info('Restored {} variables'.format(mode))

    def init_datasets(self):
        pass

    def launch_training_session(self):
        pass

    def training(self):
        pass

    def validating(self):
        pass

    def testing(self):
        pass

    def close(self):
        pass
