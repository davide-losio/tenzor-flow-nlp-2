# PESInet.
#
# Copyright (C) 2017, Davide Francesco Losio, Mauro Luchetti
#
# This file is part of PESInet.
#
#     PESInet is free software: you can redistribute it and/or modify
#     it under the terms of the GNU General Public License as published by
#     the Free Software Foundation, either version 3 of the License, or
#     (at your option) any later version.
#
#     PESInet is distributed in the hope that it will be useful,
#     but WITHOUT ANY WARRANTY; without even the implied warranty of
#     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#     GNU General Public License for more details.
#
#     You should have received a copy of the GNU General Public License
#     along with PESInet.  If not, see <http://www.gnu.org/licenses/>.
# ==========================================================================


from configparser import ConfigParser

import configurations.PESInet_config_sound as Pcs
import numpy as np
import tensorflow as tf

import configurations.PESInet_config_text as Pct


class TxtSndConfig(object):
    cfg = ConfigParser()
    cfg.read('configurations/config.ini')

    mode = 'text_and_sound'

    # txt and snd modules parameters are taken from the respective configurations

    txt_cfg = Pct.txt_config
    snd_cfg = Pcs.snd_config

    whole_cfg = cfg['bays']

    shuffle_dataset = 'False'
    batch_size = int(whole_cfg['batch_size'])
    validation_batch = int(whole_cfg['validation_batch'])
    validation_size = int(whole_cfg['validation_size'])
    dataset_size = int(whole_cfg['dataset_size'])
    txt_cfg.shuffle_dataset = shuffle_dataset
    snd_cfg.shuffle_dataset = shuffle_dataset

    # dataset parameter needs to be customized
    t_train_path = txt_cfg.train_path
    s_train_path = snd_cfg.train_path

    t_test_path = txt_cfg.validation_path
    s_test_path = snd_cfg.validation_path

    t_pred_path = txt_cfg.pred_path
    s_pred_path = snd_cfg.pred_path

    train_logs = whole_cfg['train_logs']

    learning_rate = float(whole_cfg['learning_rate'])
    txt_cfg.learning_rate = learning_rate
    snd_cfg.learning_rate = learning_rate

    epochs = int(whole_cfg['epochs'])
    classes = int(whole_cfg['classes'])
    ckpt_path = whole_cfg['ckpt_path']
    snd_cfg.save = 'False'
    txt_cfg.save = 'False'
    save = whole_cfg['save_train']

    txt_cfg.dtype = tf.float32
    snd_cfg.dtype = txt_cfg.dtype

    t_conf_temp = np.fromstring(whole_cfg['t_conf'], sep=",")
    s_conf_temp = np.fromstring(whole_cfg['s_conf'], sep=",")
    calc_probs = whole_cfg['calc_probs']
    prior_probs = np.fromstring(whole_cfg['prior_probs'], sep=",")
    t_conf = []
    s_conf = []

    for i in range(classes):
        t_conf.append(t_conf_temp[(i * classes):((i + 1) * classes)])
        s_conf.append(s_conf_temp[(i * classes):((i + 1) * classes)])
