# PESInet.
#
# Copyright (C) 2017, Davide Francesco Losio, Mauro Luchetti
#
# This file is part of PESInet.
#
#     PESInet is free software: you can redistribute it and/or modify
#     it under the terms of the GNU General Public License as published by
#     the Free Software Foundation, either version 3 of the License, or
#     (at your option) any later version.
#
#     PESInet is distributed in the hope that it will be useful,
#     but WITHOUT ANY WARRANTY; without even the implied warranty of
#     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#     GNU General Public License for more details.
#
#     You should have received a copy of the GNU General Public License
#     along with PESInet.  If not, see <http://www.gnu.org/licenses/>.
# ==========================================================================

import copy


def get_sound_cfg():
    import configurations.PESInet_config_sound as Pcs
    snd_cfg = copy.deepcopy(Pcs.snd_config())
    return snd_cfg


def get_text_cfg():
    import configurations.PESInet_config_text as Pct
    txt_cfg = copy.deepcopy(Pct.txt_config())
    return txt_cfg


def get_combined_cfg():
    import configurations.PESInet_config_combined as Pcc
    comb_cfg = copy.deepcopy(Pcc.TxtSndConfig())
    return comb_cfg


def get_prob_cfg():
    import configurations.PESInet_config_bays as Pcp
    pr_cfg = copy.deepcopy(Pcp.TxtSndConfig())
    return pr_cfg
