# PESInet.
#
# Copyright (C) 2017, Davide Francesco Losio, Mauro Luchetti
#
# This file is part of PESInet.
#
#     PESInet is free software: you can redistribute it and/or modify
#     it under the terms of the GNU General Public License as published by
#     the Free Software Foundation, either version 3 of the License, or
#     (at your option) any later version.
#
#     PESInet is distributed in the hope that it will be useful,
#     but WITHOUT ANY WARRANTY; without even the implied warranty of
#     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#     GNU General Public License for more details.
#
#     You should have received a copy of the GNU General Public License
#     along with PESInet.  If not, see <http://www.gnu.org/licenses/>.
# ==========================================================================

from configparser import ConfigParser

import tensorflow as tf


class snd_config(object):
    cfg = ConfigParser()
    cfg.read('configurations/config.ini')

    snd_cfg = cfg['sound']

    mode = 'sound'
    layers = int(snd_cfg['layers'])
    train_path = snd_cfg['train_path']
    validation_path = snd_cfg['validation_path']
    test_path = snd_cfg['test_path']
    pred_path = snd_cfg['pred_path']
    ckpt_path = snd_cfg['ckpt_path']
    train_logs = snd_cfg['train_logs']
    dataset_size = int(snd_cfg['dataset_size'])
    testset_size = int(snd_cfg['testset_size'])
    validation_size = int(snd_cfg['validation_size'])
    validation_batch = int(snd_cfg['validation_batch'])
    n_of_features = int(snd_cfg['n_of_features'])
    classes = int(snd_cfg['classes'])
    epochs = int(snd_cfg['epochs'])
    batch_size = int(snd_cfg['batch_size'])
    learning_rate = float(snd_cfg['learning_rate'])
    k = int(snd_cfg['k'])
    dtype = tf.float32
    save = snd_cfg['save_train']
    shuffle_dataset = str(snd_cfg['shuffle_dataset'])
    n_hidden = []

    def __init__(self):
        for i in range(self.layers):
            self.n_hidden.append(int(self.snd_cfg['layer_' + str(i + 1)]))
