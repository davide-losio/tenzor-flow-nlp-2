# PESInet.
#
# Copyright (C) 2017, Davide Francesco Losio, Mauro Luchetti
#
# This file is part of PESInet.
#
#     PESInet is free software: you can redistribute it and/or modify
#     it under the terms of the GNU General Public License as published by
#     the Free Software Foundation, either version 3 of the License, or
#     (at your option) any later version.
#
#     PESInet is distributed in the hope that it will be useful,
#     but WITHOUT ANY WARRANTY; without even the implied warranty of
#     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#     GNU General Public License for more details.
#
#     You should have received a copy of the GNU General Public License
#     along with PESInet.  If not, see <http://www.gnu.org/licenses/>.
# ==========================================================================

from configparser import ConfigParser

import tensorflow as tf


class txt_config(object):
    cfg = ConfigParser()
    cfg.read('configurations/config.ini')

    txt_cfg = cfg['text']

    mode = 'text'
    layers = int(txt_cfg['layers'])
    train_path = txt_cfg['train_path']
    validation_path = txt_cfg['validation_path']
    test_path = txt_cfg['test_path']
    pred_path = txt_cfg['pred_path']
    ckpt_path = txt_cfg['ckpt_path']
    train_logs = txt_cfg['train_logs']
    dataset_size = int(txt_cfg['dataset_size'])
    validation_size = int(txt_cfg['validation_size'])
    validation_batch = int(txt_cfg['validation_batch'])
    testset_size = int(txt_cfg['testset_size'])
    n_of_features = int(txt_cfg['n_of_features'])
    classes = int(txt_cfg['classes'])
    epochs = int(txt_cfg['epochs'])
    batch_size = int(txt_cfg['batch_size'])
    learning_rate = float(txt_cfg['learning_rate'])
    k = int(txt_cfg['k'])
    dtype = tf.float32
    save = txt_cfg['save_train']
    shuffle_dataset = str(txt_cfg['shuffle_dataset'])
    n_hidden = []

    def __init__(self):
        for i in range(self.layers):
            self.n_hidden.append(int(self.txt_cfg['layer_' + str(i + 1)]))
