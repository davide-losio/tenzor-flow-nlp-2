# PESInet.
#
# Copyright (C) 2017, Davide Francesco Losio, Mauro Luchetti
#
# This file is part of PESInet.
#
#     PESInet is free software: you can redistribute it and/or modify
#     it under the terms of the GNU General Public License as published by
#     the Free Software Foundation, either version 3 of the License, or
#     (at your option) any later version.
#
#     PESInet is distributed in the hope that it will be useful,
#     but WITHOUT ANY WARRANTY; without even the implied warranty of
#     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#     GNU General Public License for more details.
#
#     You should have received a copy of the GNU General Public License
#     along with PESInet.  If not, see <http://www.gnu.org/licenses/>.
# ==========================================================================

import glob
import sys

import numpy as np
import pandas
import tensorflow as tf

q_path = '/*.csv'
e_path = '/*.csv'
s_path = '/*.csv'
out_file_name = 'Smoothed.tfrecords'

FEATURES_SELECTION_LIST = []
NUM_OF_FEATURES_COLUMNS = 129
NUM_OF_TARGETS = 3


def apply_selection(_list, mask):
    '''
    Use this function to apply a mask to the feature rows.
    If the mask is an empty list the whole _list object is returned.
    Use FEATURES_SELECTION_LIST at this purpouse.

    E.g.: FEATURES_SELECTION_LIST = [[1, 5], [8, 10]
    This mask takes elements 1, 2, 3, 4, 5, 8, 9, 10 of _list.
    Both the starting and ending indexes are taken in the returned list.
    '''

    if not mask:
        return _list[:NUM_OF_FEATURES_COLUMNS]
    result = []
    for el in mask:
        s_idx = el[0]
        e_idx = el[1] + 1
        result += _list[s_idx:e_idx]
    return result


def get_row_binary(file):
    csv = pandas.read_csv(file, engine='python').values

    features = []
    labels = []

    for row in csv:
        features_list = apply_selection(list(row), FEATURES_SELECTION_LIST)
        features.append(list(features_list))
        labels.append(list(row[
                           NUM_OF_FEATURES_COLUMNS: NUM_OF_FEATURES_COLUMNS + NUM_OF_TARGETS]))

    features = np.array(features, np.float32)
    labels = np.array(labels, np.float32)

    return features.tobytes(), labels.tobytes()


def write_to_tfrecord(features, labels, writer):
    # write label, shape, and image content to the TFRecord file

    example = tf.train.Example(features=tf.train.Features(feature={
        'labels': tf.train.Feature(bytes_list=tf.train.BytesList(
            value=[labels])),
        'features': tf.train.Feature(bytes_list=tf.train.BytesList(
            value=[features]))
    }))

    writer.write(example.SerializeToString())


if __name__ == '__main__':

    # take a list of folder path as argument, otherwise use the default one
    if len(sys.argv) > 1:
        args = sys.argv[1:]
        args = [arg + '*.csv' for idx, arg in enumerate(args) if idx != 3]
        args += [sys.argv[4]]
        print(args)
    else:
        args = [s_path, e_path, q_path, out_file_name]

    counter = 0
    notf = []

    files = glob.glob(args[0])
    files += glob.glob(args[1])
    files += glob.glob(args[2])
    writer = tf.python_io.TFRecordWriter(args[3])

    # iterate over the list getting each file

    for file in files:
        print('procesing file name:', file)
        feat_byn, lab_byn = get_row_binary(file)
        if len(feat_byn) != 0 and len(lab_byn) != 0:
            write_to_tfrecord(feat_byn, lab_byn, writer)
    writer.close()
