import glob
import os
import random
import shutil

path = "/home/arcslab/Desktop/PESInet_MauroLuchetti_DavideLosio/rtmp/parsed_files/Statements/*.txt"
dest_path = "/home/arcslab/Desktop/PESInet_MauroLuchetti_DavideLosio/rtmp/parsed_files/Out_Statements/"
number_of_samples = 10000
a_seed = 24

files = glob.glob(path)
n_of_elems = int(number_of_samples)
random.seed(a=a_seed)
choosen_elem = random.sample(population=files, k=n_of_elems)

for elem in choosen_elem:
    print('taking elem:', elem)
    # elem_clean_path = elem.replace('\\', '/') for windows
    (head, filenameTXT) = os.path.split(elem)
    filenameSND = filenameTXT.split(".")[0] + ".wav"
    shutil.move(head + "/" + filenameTXT, dest_path + filenameTXT)
    shutil.move(head + "/" + filenameSND, dest_path + filenameSND)
