# PESInet.
#
# Copyright (C) 2017, Davide Francesco Losio, Mauro Luchetti
#
# This file is part of PESInet.
#
#     PESInet is free software: you can redistribute it and/or modify
#     it under the terms of the GNU General Public License as published by
#     the Free Software Foundation, either version 3 of the License, or
#     (at your option) any later version.
#
#     PESInet is distributed in the hope that it will be useful,
#     but WITHOUT ANY WARRANTY; without even the implied warranty of
#     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#     GNU General Public License for more details.
#
#     You should have received a copy of the GNU General Public License
#     along with PESInet.  If not, see <http://www.gnu.org/licenses/>.
# ==========================================================================

import csv as c
import glob

import pandas

csv_path = '/Users/Mauro/Desktop/soniaset/csv/*.csv'
csv_out_path = '/Users/Mauro/Desktop/soniaset/csv_clean/'

treshold = 1e-3


def search_start_index(table):
    for i in range(len(table)):
        if table[i][2] != 0.0:
            init_index = i
            break
        else:
            continue
    return init_index


def search_end_index(table):
    for i in range(len(table) - 1, 0, -1):
        if table[i][2] != 0.0:
            final_index = i
            break
        else:
            continue
    return final_index


def filter_csv(table):
    init_index = search_start_index(table)
    end_index = search_end_index(table)
    ok_rows = []
    for idx, row in enumerate(table):
        if (idx >= init_index and idx <= end_index):
            ok_rows.append(row)
    return ok_rows


def write_csv(file_name, rows_to_write):
    with open(file_name, 'w', newline='') as csvfile:
        writer = c.writer(csvfile, delimiter=',')
        for row in rows_to_write:
            writer.writerow(row)


if __name__ == '__main__':
    files = glob.glob(csv_path)

    for csv in files:
        file_name = csv.split('/')[-1]
        print('Processing: ', file_name)
        table = pandas.read_csv(csv, engine='python').values
        ok_rows = filter_csv(table)
        write_csv(csv_out_path + file_name, ok_rows)
