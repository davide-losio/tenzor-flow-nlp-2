# PESInet.
#
# Copyright (C) 2017, Davide Francesco Losio, Mauro Luchetti
#
# This file is part of PESInet.
#
#     PESInet is free software: you can redistribute it and/or modify
#     it under the terms of the GNU General Public License as published by
#     the Free Software Foundation, either version 3 of the License, or
#     (at your option) any later version.
#
#     PESInet is distributed in the hope that it will be useful,
#     but WITHOUT ANY WARRANTY; without even the implied warranty of
#     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#     GNU General Public License for more details.
#
#     You should have received a copy of the GNU General Public License
#     along with PESInet.  If not, see <http://www.gnu.org/licenses/>.
# ==========================================================================

import glob
import os
import sys

import gensim
import numpy as np
import tensorflow as tf

q_path = '/Users/Mauro/Desktop/root_path_for_doubled_dataset/out/text/TestSet/Questions/*.txt'
e_path = '/Users/Mauro/Desktop/root_path_for_doubled_dataset/out/text/TestSet/Exclamations/*.txt'
s_path = '/Users/Mauro/Desktop/root_path_for_doubled_dataset/out/text/TestSet/Statements/*.txt'
out_file_name = '/Users/Mauro/Desktop/root_path_for_doubled_dataset/out/text/TestSet/Smoothed.tfrecords'


def write_to_tfrecord(label, binary_text, writer):
    example = tf.train.Example(features=tf.train.Features(feature={
        'labels': tf.train.Feature(bytes_list=tf.train.BytesList(value=[label])),
        'features': tf.train.Feature(bytes_list=tf.train.BytesList(value=[binary_text]))
    }))

    writer.write(example.SerializeToString())


def get_text_bynary(text, label, model):
    counter = 0
    translated_text = []
    lab = []
    notfounded = []

    for word in text:
        if word == 'è':
            word = word.replace('è', "e'")
        counter += 1
        try:
            translated_text.append(model.wv[word])
        except KeyError:
            translated_text.append(model.wv['unknown'])
            notfounded.append(word)

    lab.append(label)
    text = np.array(translated_text, np.float32)
    label = np.array(lab, np.float32)
    return text.tobytes(), label.tobytes(), notfounded, counter


def make_label(file):
    (head, _) = os.path.split(file)
    (_, label) = os.path.split(head)

    print(label)

    # 001, questions, 010 exclamations, 100 statements
    if label == 'Questions':
        one_hot_lab = [0, 0, 1]
    elif label == 'Statements':
        one_hot_lab = [1, 0, 0]
    elif label == 'Exclamations':
        one_hot_lab = [0, 1, 0]
    else:
        raise ("the fuck is goin' on")

    return (one_hot_lab, label)


if __name__ == '__main__':

    local_path = os.path.dirname(__file__)

    # take a list of folder path as argument, otherwise use the default one
    if len(sys.argv) > 1:
        args = sys.argv[1:]
        args = [arg + '*.txt' for idx, arg in enumerate(args) if idx != 3]
        args += [sys.argv[4]]
        print(args)
    else:
        args = [s_path, e_path, q_path, out_file_name]

    counter = 0
    notf = []

    model = gensim.models.Word2Vec.load(local_path + '/W2V_model/glove_WIKI')
    files = glob.glob(args[0])
    files += glob.glob(args[1])
    files += glob.glob(args[2])
    writer = tf.python_io.TFRecordWriter(args[3])

    # iterate over the list getting each file

    for file in files:
        # open the file and then call .read() to get the text
        with open(file, encoding='utf-8') as f:
            (one_hot_lab, label) = make_label(file)
            print('procesing file name:', file, 'Label:', label, 'One Hot Repr:', one_hot_lab)
            text = f.read().split()
            text_byn, oh_lab_byn, nf, cnt = get_text_bynary(text, one_hot_lab, model)
            counter += cnt
            if nf:
                notf.append(nf)
            if len(text_byn) != 0:
                write_to_tfrecord(oh_lab_byn, text_byn, writer)
    writer.close()

    print("tot word:", counter)
    print("words not found:", len(notf))
    print(notf)
