# PESInet.
#
# Copyright (C) 2017, Davide Francesco Losio, Mauro Luchetti
#
# This file is part of PESInet.
#
#     PESInet is free software: you can redistribute it and/or modify
#     it under the terms of the GNU General Public License as published by
#     the Free Software Foundation, either version 3 of the License, or
#     (at your option) any later version.
#
#     PESInet is distributed in the hope that it will be useful,
#     but WITHOUT ANY WARRANTY; without even the implied warranty of
#     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#     GNU General Public License for more details.
#
#     You should have received a copy of the GNU General Public License
#     along with PESInet.  If not, see <http://www.gnu.org/licenses/>.
# ==========================================================================

import glob

import pandas

num_of_features = 6

csv_path = '/Users/Mauro/PycharmProjects/tenzor-flow-nlp-2/sound/silenced_csv/Statements/*.csv'
csv_out_path = '/Users/Mauro/PycharmProjects/tenzor-flow-nlp-2/sound/normalized_csv/Statements/'


def normalize(data_frame, columns_indexes):
    for col in columns_indexes:
        df[col] = (df[col] - df[col].mean()) / df[col].std(ddof=0)


if __name__ == '__main__':
    files = glob.glob(csv_path)

    for csv in files:
        file_name = csv.split('/')[-1]
        print('Processing: ', file_name)
        df = pandas.read_csv(filepath_or_buffer=csv,
                             header=None)
        cols = list(df.columns)
        features_cols = cols[:num_of_features]

        normalize(df, features_cols)

        df.to_csv(path_or_buf=csv_out_path + file_name,
                  header=False,
                  index=False)
