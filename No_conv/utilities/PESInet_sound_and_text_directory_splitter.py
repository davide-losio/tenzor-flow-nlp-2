# PESInet.
#
# Copyright (C) 2017, Davide Francesco Losio, Mauro Luchetti
#
# This file is part of PESInet.
#
#     PESInet is free software: you can redistribute it and/or modify
#     it under the terms of the GNU General Public License as published by
#     the Free Software Foundation, either version 3 of the License, or
#     (at your option) any later version.
#
#     PESInet is distributed in the hope that it will be useful,
#     but WITHOUT ANY WARRANTY; without even the implied warranty of
#     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#     GNU General Public License for more details.
#
#     You should have received a copy of the GNU General Public License
#     along with PESInet.  If not, see <http://www.gnu.org/licenses/>.
# ==========================================================================

import glob
import os
import random
import shutil
import sys

root_path = '/Users/Mauro/Desktop/root_path_for_doubled_dataset/in/'

sound_path = 'csv/'
text_path = 'text/'

text_out_path = 'text/'
sound_out_path = 'audio/'

q_path = 'Questions/'
e_path = 'Exclamations/'
s_path = 'Statements/'

dest_path = '/Users/Mauro/Desktop/root_path_for_doubled_dataset/out/'


def create_correct_couples(path):
    current_text_path = root_path + path + text_path
    current_sound_path = root_path + path + sound_path

    list_of_text_files = glob.glob(current_text_path + '*.txt')
    list_of_sound_files = glob.glob(current_sound_path + '*.csv')

    random.shuffle(list_of_sound_files)

    half_index = int(len(list_of_sound_files) / 2)

    correct_half = list_of_sound_files[:half_index]
    counterexample_half = list_of_sound_files[half_index:]

    for file in correct_half:
        file_name = file.split('/')[-1]
        print('Processing: ', file_name)
        corresponding_text_file = current_text_path + file_name.split('.')[0] + '.txt'
        try:
            shutil.copy(corresponding_text_file, dest_path + text_out_path + path)
            shutil.copy(file, dest_path + sound_out_path + path)
            list_of_text_files.remove(corresponding_text_file)
        except FileNotFoundError:
            print("########## FileNotFoundError")
            continue

    return counterexample_half, list_of_text_files


def handle_class_counterexamples(sound_list, text_list, path):
    idx = 0
    for statement in sound_list:
        file_name = statement.split('/')[-1]
        print('Processing: ', file_name)
        corresponding_text_file = text_list[idx]
        try:
            shutil.copy(corresponding_text_file, dest_path + text_out_path + path)
            old_text_file_name = corresponding_text_file.split('/')[-1]
            new_text_file_name = file_name.split('.')[0] + '.txt'
            os.rename(dest_path + text_out_path + path + old_text_file_name,
                      dest_path + text_out_path + path + new_text_file_name)
            shutil.copy(statement, dest_path + sound_out_path + path)
            idx += 1
        except FileNotFoundError:
            print("########## FileNotFoundError")
            continue


def create_counterexamples(sound_files, text_files):
    statements_sound_files = [file for file in sound_files if file.split('/')[-1].split('.')[0].split('_')[1] == '0']
    exclamations_sound_files = [file for file in sound_files if file.split('/')[-1].split('.')[0].split('_')[1] == '1']
    questions_sound_files = [file for file in sound_files if file.split('/')[-1].split('.')[0].split('_')[1] == '2']

    print(statements_sound_files)
    print(exclamations_sound_files)
    print(questions_sound_files)

    statements_text_files = [file for file in text_files if file.split('/')[-1].split('.')[0].split('_')[1] == '0']
    exclamations_text_files = [file for file in text_files if file.split('/')[-1].split('.')[0].split('_')[1] == '1']
    questions_text_files = [file for file in text_files if file.split('/')[-1].split('.')[0].split('_')[1] == '2']

    text_available_for_statements = exclamations_text_files + questions_text_files
    text_available_for_exclamations = statements_text_files + questions_text_files
    text_available_for_questions = statements_text_files + exclamations_text_files

    random.shuffle(text_available_for_statements)
    random.shuffle(text_available_for_exclamations)
    random.shuffle(text_available_for_questions)

    handle_class_counterexamples(statements_sound_files, text_available_for_statements, s_path)
    handle_class_counterexamples(exclamations_sound_files, text_available_for_exclamations, e_path)
    handle_class_counterexamples(questions_sound_files, text_available_for_questions, q_path)


if __name__ == '__main__':

    # take a list of folder path as argument, otherwise use the default one
    if len(sys.argv) > 1:
        paths = sys.argv[1:]
    else:
        paths = [q_path, s_path, e_path]

    sound_files_still_available = []
    text_files_still_available = []

    # first iterate over classes and take half of samples as both audio and text files
    for path in paths:
        print('Checking: ', path)
        lists_of_files = create_correct_couples(path)

        # for each class save the list of not used files
        sound_files_still_available += lists_of_files[0]
        text_files_still_available += lists_of_files[1]

    # finally generate the second half of dataset using the lists obtained to create counterexamples
    create_counterexamples(sound_files_still_available, text_files_still_available)
