Opzioni da dare quando viene lanciato il programma:

-txt- for text features
-snd- for sound features
-st-  for sound and text features together
-stp- for sound and text probabilistic

-CV- for cross validation
-TR- for train
-P-  for prediction

Gli iperparametri vanno settati nel file config.ini, c'� una serie di iperparametri
per ognuna delle tre modalit� delle reti. � Importante, ai fini del funzionamento, non
cambiare i parametri dei singoli moduli quando vengono uniti nella rete cobinata, audio
pi� testo. Ovviamente questo pregiudicherebbe il funzionamento.

Per quel che riguarda la rete, CV � stato testato solo molto tempo fa, e solo sui singoli
moduli. Si consiglia di controllare e testare sia per il funzionamento coi singoli moduli
che ovviamente per la rete combinata. Data la modularit� del codice, a livello teorico,
il funzionamento coi singoli moduli dovrebbe garantire anche il funzionamento della rete 
composta.

Per ci� che riguarda gli iter di utilizzo allenamento:

1 - trainare i singoli moduli separatamente, o la configurazione combinata a scelta.
    settare save checkpoint True, sugli iper parametri se si desidera salvare l'apprendimento.

2 - i checkpoint file che permettono di ri-utilizzare la rete trainata saranno posti 
    nella cartella ckpt.

3 - la prediction session si occuper� automaticamente di effettuare predizione utilizzando
    i pesi e i parametri salvati nei checkpoint posti nella cartella ckpt.

� FONDAMENTALE che la rete in prediction sia esattamente uguale a quella trainata, altrimenti
non si potra procedere al restore automatico dei parametri appresi durante il training.

N.B la cartella di logs veniva usata per salvare i dati provenienti dal monitoraggio della
    rete con tensorboard, che � stato disattivato nella versione finale. 