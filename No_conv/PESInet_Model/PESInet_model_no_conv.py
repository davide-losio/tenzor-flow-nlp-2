# PESInet.
#
# Copyright (C) 2017, Davide Francesco Losio, Mauro Luchetti
#
# This file is part of PESInet.
#
#     PESInet is free software: you can redistribute it and/or modify
#     it under the terms of the GNU General Public License as published by
#     the Free Software Foundation, either version 3 of the License, or
#     (at your option) any later version.
#
#     PESInet is distributed in the hope that it will be useful,
#     but WITHOUT ANY WARRANTY; without even the implied warranty of
#     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#     GNU General Public License for more details.
#
#     You should have received a copy of the GNU General Public License
#     along with PESInet.  If not, see <http://www.gnu.org/licenses/>.
# ==========================================================================

import tensorflow as tf


def high_level_feat(cfg, features, keep_prob):

    next_input = features
    lengths = length(features)

    output_fw, output_bw = blstm_layer(cfg, next_input, 0, keep_prob, lengths)

    with tf.variable_scope('preparing_last_layer_input'):
        last_fw = _last_relevant(output_fw, lengths)
        last_bw = output_bw[:, 0, :]
        last = tf.concat([last_fw, last_bw], 1)

    return last


def make_cell(n_units, input_size, keep_prob):
    cell = tf.nn.rnn_cell.LSTMCell(n_units)
    return tf.nn.rnn_cell.DropoutWrapper(cell,
                                         input_keep_prob=keep_prob,
                                         output_keep_prob=keep_prob,
                                         state_keep_prob=keep_prob,
                                         variational_recurrent=True,
                                         dtype=tf.float32,
                                         input_size=input_size)


def blstm_layer(cfg, _input, idx, keep_prob, lengths):

    input_dim = []
    input_dim.append(cfg.n_of_features)
    for i in range(cfg.layers):
        input_dim.append(cfg.n_hidden[i])
    n_layers = cfg.layers

    with tf.variable_scope('BLSTM-' + str(idx)):
        fw_layer = \
            tf.nn.rnn_cell.MultiRNNCell([make_cell(cfg.n_hidden[i], input_dim[i], keep_prob) for i in range(n_layers)])
        bw_layer = \
            tf.nn.rnn_cell.MultiRNNCell([make_cell(cfg.n_hidden[i], input_dim[i], keep_prob) for i in range(n_layers)])

        (output_fw, output_bw), _ = tf.nn.bidirectional_dynamic_rnn(
            fw_layer,
            bw_layer,
            _input,
            dtype=cfg.dtype,
            sequence_length=lengths
        )

    return output_fw, output_bw


def logits(cfg, last, in_dimension):

    with tf.variable_scope('Logits'):
        w_log = weight_variable(name='w_log',
                                shape=[in_dimension, cfg.classes])

        b_log = bias_variable(name='b_log',
                              shape=[cfg.classes])

        _logits = tf.add(tf.matmul(last, w_log), b_log, name='logits')

    return _logits, (w_log, b_log)


def weight_variable(shape, name):
    with tf.device('/cpu:0'):
        with tf.variable_scope("Weights_" + name):
            w = tf.get_variable(name, shape=shape, initializer=tf.contrib.layers.xavier_initializer())
    return w


def bias_variable(shape, name):
    with tf.device('/cpu:0'):
        initial = tf.constant(0.1, shape=shape)
        b = tf.Variable(initial, name=name)
    return b


def length(sequence):
    used = tf.sign(tf.reduce_max(tf.abs(sequence), reduction_indices=2))
    _length = tf.reduce_sum(used, reduction_indices=1)
    _length = tf.cast(_length, tf.int32)
    return _length


def _last_relevant(output, _length):
    batch_size = tf.shape(output)[0]
    max_length = tf.shape(output)[1]
    output_size = tf.shape(output)[2]
    index = tf.range(0, batch_size) * max_length + (_length - 1)
    flat = tf.reshape(output, [-1, output_size])
    relevant = tf.gather(flat, index)
    return relevant


def accuracy(labels, _logits, name='accuracy'):
    with tf.variable_scope("my_metrics"):
        acc, a_up = tf.metrics.accuracy(labels=tf.argmax(labels, 1), predictions=tf.argmax(_logits, 1), name=name)
    return acc, a_up


def probabilities(_logits):
    return tf.nn.softmax(_logits, name="softmax_tensor")


def loss(labels, _logits):
    _loss = tf.losses.softmax_cross_entropy(
        onehot_labels=labels, logits=_logits)
    return _loss


def train_op(cfg, _loss):
    optimizer = tf.train.AdamOptimizer(cfg.learning_rate)
    return optimizer.minimize(_loss)


def train_only_on_vars(cfg, _loss, var_list):
    optimizer = tf.train.AdamOptimizer(cfg.learning_rate)
    return optimizer.minimize(_loss, var_list=var_list)


def confusion_mat(labels, _logits):
    return tf.confusion_matrix(tf.argmax(labels, 1), tf.argmax(_logits, 1))


def precision(labels, _logits):
    with tf.variable_scope("my_metrics"):
        precision_stuff = tf.metrics.precision(labels=tf.argmax(labels, 1),
                                               predictions=tf.argmax(_logits, 1))
    return precision_stuff


def recall(labels, _logits):
    with tf.variable_scope("my_metrics"):
        recall_stuff = tf.metrics.recall(labels=tf.argmax(labels, 1),
                                         predictions=tf.argmax(_logits, 1))
    return recall_stuff


def f1_score(prec, rec):
    num = tf.multiply(prec, rec)
    den = tf.add(prec, rec)
    f1 = tf.multiply(2.0, tf.div(num, den))

    return f1


def inc_loss(old_loss, new_loss, count):
    old_count = count - 1
    old_avg = tf.multiply(old_loss, old_count)
    new_avg = tf.add(old_avg, new_loss)
    new_avg = new_avg / count
    return new_avg
